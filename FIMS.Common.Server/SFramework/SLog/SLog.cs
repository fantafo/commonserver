﻿using System.Collections.Generic;
using System.Text;
using System;
using System.IO;

public class SLog
{
    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // STATIC
    //
    /////////////////////////////////////////////////////////////////////////////////////////////

    public static SLog _default;
    public static SLog Default { get { return _default ?? (_default = new SLog("SLog")); } }

    static string ToTag(object tag) { return tag != null ? tag.ToString() : Default.tag; }
    static string ToMsg(object msg) { return msg != null ? msg.ToString() : " "; }

    public static void d(object message) { Default.debug(ToTag(null), ToMsg(message)); }
    public static void d(object tag, object message) { Default.debug(ToTag(tag), ToMsg(message)); }
    public static void df(object tag, object message, params object[] args) { Default.debug(ToTag(tag), string.Format(ToMsg(message), args)); }

    public static void w(object message) { Default.warn(ToTag(null), ToMsg(message)); }
    public static void w(object tag, object message) { Default.warn(ToTag(tag), ToMsg(message)); }

    public static void e(object message) { Default.err(ToTag(null), ToMsg(message)); }
    public static void e(object tag, object message) { Default.err(ToTag(tag), ToMsg(message)); }
    public static void e(Exception ex) { Default.ex(ToTag(null), ex); }
    public static void e(object tag, Exception ex) { Default.ex(ToTag(tag), ex); }

    /////////////////////////////////////////////////////////////////////////////////////////////
    //
    // MEMBER
    //
    /////////////////////////////////////////////////////////////////////////////////////////////

    public string tag = "SLog";

    public SLog(string tag)
    {
        this.tag = tag;
    }
    public SLog(object tag)
    {
        this.tag = tag.GetType().Name;
    }


    public void debug(string message) { debug(tag, message); }
    public void debugF(string tag, string message, params string[] args) { debug(tag, string.Format(message, args)); }
    public void debug(string tag, string message)
    {
#if DEBUG || PRINT_LOG
        Console.WriteLine(string.Format("{0:HH:mm:ss,fff} D [{1,-20}] {2}", DateTime.Now, tag, message));
#endif
#if FILE_LOG
        writeFile('D', tag, message);
#endif
    }

    public void warn(string message) { warn(tag, message); }
    public void warn(string tag, string message)
    {
#if DEBUG || PRINT_LOG
        Console.WriteLine(string.Format("{0:HH:mm:ss,fff} W [{1,-20}] {2}", DateTime.Now, tag, message));
#endif
#if FILE_LOG
        writeFile('W', tag, message);
#endif
    }

    public void err(string message) { err(tag, message); }
    public void err(string tag, string message)
    {
#if DEBUG || PRINT_LOG
        Console.WriteLine(string.Format("{0:HH:mm:ss,fff} E [{1,-20}] {2}", DateTime.Now, tag, message));
#endif
#if FILE_LOG
        writeFile('E', tag, message);
#endif
    }

    public void ex(Exception e) { ex(tag, e); }
    public void ex(string tag, Exception e)
    {
#if DEBUG || PRINT_LOG
        Console.WriteLine(string.Format("{0:HH:mm:ss,fff} E [{1,-20}] {2}", DateTime.Now, tag, e.Message));
        Console.WriteLine(e.StackTrace);
#endif
#if FILE_LOG
        writeFile('E', tag, e.Message+"\r\n"+e.StackTrace);
#endif
    }

#if FILE_LOG
    static object lockObj = new object();
    static StreamWriter fw;
    static FileInfo file;
    static DateTime fileDate;
    public static void writeFile(char type, string tag, string message)
    {
        const string logPath = "./log/log.txt";
        lock (lockObj)
        {
            DateTime now = DateTime.Now;

            if (fw != null && fileDate.Day != now.Day)
            {
                fw.Dispose();
                fw = null;

                if (File.Exists(logPath))
                {
                    for (int i = 0; i < 10000; i++)
                    {
                        string path = string.Format("./log/{0:yyyyMMdd-HHmmss}.{1}.txt", fileDate, i);
                        if (!File.Exists(path))
                        {
                            File.Move(logPath, path);
                            break;
                        }
                    }
                }
            }

            if (fw == null)
            {
                fileDate = DateTime.Now;
                if (File.Exists(logPath))
                {
                    for (int i = 0; i < 10000; i++)
                    {
                        string path = string.Format("./log/{0:yyyyMMdd-HHmmss}.{1}.txt", fileDate, i);
                        if (!File.Exists(path))
                        {
                            File.Move(logPath, path);
                            break;
                        }
                    }
                }
                else if (!Directory.Exists("./log"))
                {
                    Directory.CreateDirectory("./log");
                }
                fw = new StreamWriter(new FileStream(logPath, FileMode.CreateNew, FileAccess.Write, FileShare.Read), Encoding.UTF8);
                file = new FileInfo(logPath);
            }

            fw.WriteLine(string.Format("{0:HH:mm:ss.fff}\t{1}\t{2,-20}\t{3}", DateTime.Now, type, tag, message));
            fw.Flush();
        }
    }
#endif
}
