﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public static class BitHelper
{
    static int[] BITS = new int[32];
    static BitHelper()
    {
        for (int i = 0; i < BITS.Length; i++)
        {
            BITS[i] = 1 << i;
        }
    }

    public static int IndexOf(this int v)
    {
        for (int i = 0; i < 32; i++)
        {
            if ((v & BITS[i]) != 0)
            {
                return i;
            }
        }
        return -1;
    }

    public static bool Has(this int v, int c)
    {
        return (v & c) != 0;
    }
}
