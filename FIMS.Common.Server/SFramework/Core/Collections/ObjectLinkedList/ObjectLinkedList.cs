﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

//#define LOG

using System.Collections;
using System.Collections.Generic;

namespace SFramework
{
    public class ObjectLinkedList<T> : IEnumerable<T>, ICollection<T>
        where T : IObjectLinkedListNode<T>
    {
        public T NULL = default(T);

        private int _count;

        public T First { get; private set; }
        public T Last { get; private set; }

        public int Count { get { return _count; } }
        public bool IsReadOnly { get { return false; } }
        public bool IsSynchronized { get { return true; } }
        public object SyncRoot { get { return false; } }


        public void Add(T item)
        {
            AddLast(item);
        }
        public void AddFirst(T item)
        {
            if (First != null)
            {
                First.Prev = item;
                item.Next = First;
            }
            else
            {
                item.Next = NULL;
                Last = item;
            }

            item.Prev = NULL;
            item.parent = this;
            First = item;
            _count++;
        }
        public void AddLast(T item)
        {
            if (Last != null)
            {
                Last.Next = item;
                item.Prev = Last;
            }
            else
            {
                item.Prev = NULL;
                First = item;
            }

            item.Next = NULL;
            item.parent = this;
            Last = item;
            _count++;
        }



        public void Clear()
        {
            First = NULL;
            Last = NULL;
            _count = 0;
        }
        public void ClearDeep()
        {
            if (Count == 0) return;

            IObjectLinkedListNode<T> item = First, temp;
            while (item != null)
            {
                temp = item.Next;
                item.Next = NULL;
                item.Prev = NULL;
                item.parent = null;
                item = temp;
            }

            First = NULL;
            Last = NULL;
            _count = 0;
        }

        public bool Contains(T item)
        {
            return item.parent == this;
        }

        public bool ContainsDeep(T item)
        {
            if (item.parent == this)
            {
                IObjectLinkedListNode<T> temp = First;
                while (temp != null)
                {
                    if (object.Equals(temp, item)) return true;
                    temp = temp.Next;
                }
            }
            return false;
        }

        public IEnumerator<T> GetEnumerator()
        {
            if (_count != 0)
            {
                T current = (T)First;
                do
                {
                    yield return current;
                } while ((current = (T)current.Next) != null);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            if (_count != 0)
            {
                T current = (T)First;
                do
                {
                    yield return current;
                } while ((current = (T)current.Next) != null);
            }
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (_count != 0)
            {
                foreach (T t in this)
                {
                    array[arrayIndex++] = t;
                }
            }
        }

        public bool Remove(T item)
        {
            if (item.parent == this)
            {
                if (ReferenceEquals(First, item))
                {
                    if (item.Next != null)
                    {
                        First = item.Next;
                        First.Prev = NULL;
                    }
                    else
                    {
                        First = NULL;
                        Last = NULL;
                    }
                }
                else if (ReferenceEquals(Last, item))
                {
                    if (item.Prev != null)
                    {
                        Last = item.Prev;
                        Last.Next = NULL;
                    }
                    else
                    {
                        First = NULL;
                        Last = NULL;
                    }
                }
                else
                {
                    item.Prev.Next = item.Next;
                    item.Next.Prev = item.Prev;
                }

                item.Prev = NULL;
                item.Next = NULL;
                item.parent = null;
                _count--;
                return true;
            }
            return false;
        }

        public T[] ToArray()
        {
            int cnt = Count;
            T[] result = new T[cnt];
            int i = 0;
            var e = GetEnumerator();
            while (e.MoveNext() && i < cnt)
            {
                result[i++] = e.Current;
            }
            return result;
        }
    }
}