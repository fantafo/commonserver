﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

//using System;
//using System.Collections.Generic;
using System.Reflection;

namespace System.Collections.Generic
{
    public class ListPool<T>
    {
        public static int MAXIMUM_POOL_SIZE = 50;

        static object LOCK = new object();
        private static Stack<List<T>> pool;

        public static List<T> Take()
        {
            lock (LOCK)
            {
                if (pool == null)
                {
                    pool = new Stack<List<T>>();
                    ListPoolHelper.AddPool(pool);
                }

                if (pool.Count == 0)
                {
                    return new List<T>();
                }
                else
                {
                    return pool.Pop();
                }
            }
        }

        public static void Release(List<T> list)
        {
            lock (LOCK)
            {
                if (pool == null)
                    return;
                if (pool.Count < MAXIMUM_POOL_SIZE)
                {
                    pool.Push(list);
                    list.Clear();
                }
            }
        }

        public static void Clear()
        {
            lock (LOCK)
            {
                pool.Clear();
            }
        }
        public static void ClearNull()
        {
            lock (LOCK)
            {
                pool = null;
            }
        }
    }

    public static class ListPoolHelper
    {
        internal static List<object> pools = new List<object>();

        public static void AddPool(object pool)
        {
            pools.Add(pool);
        }

        public static void ClearAll()
        {
            for (int i = 0; i < pools.Count; i++)
            {
                object pool = pools[i];

                Type type = pool.GetType();
                MethodInfo m = type.GetMethod("Clear", BindingFlags.Static | BindingFlags.Public);
                m.Invoke(pool, null);
            }
        }

        public static void ClearAllNull()
        {
            for (int i = 0; i < pools.Count; i++)
            {
                object pool = pools[i];

                Type type = pool.GetType();
                MethodInfo m = type.GetMethod("ClearNull", BindingFlags.Static | BindingFlags.Public);
                m.Invoke(pool, null);
            }
        }

        public static void Release<T>(this List<T> list)
        {
            ListPool<T>.Release(list);
        }
    }
}