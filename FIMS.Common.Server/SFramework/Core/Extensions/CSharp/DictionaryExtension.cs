﻿// Copyright (c) 2016 Sions
// 
// SFramework version 1.0.0
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;


public static class DictionaryExtension
{
    #region Collection Extends
    /// <summary>
    /// Contains Check Add.
    /// </summary>
    public static void Put<K, T>(this Dictionary<K, T> dict, K key, T val)
    {
        if (dict.ContainsKey(key))
        {
            dict[key] = val;
        }
        else
        {
            dict.Add(key, val);
        }
    }
    /// <summary>
    /// 키가 이미 존재 할 경우에만 값을 교체합니다. 키가 들어있지 않은 경우에는 무시합니다.
    /// </summary>
    /// <returns>이미 키값이 존재하여 교체에 성공했을 때 True를 반환합니다.</returns>
    public static bool ChangeValue<K, T>(this Dictionary<K, T> dic, K key, T value)
    {
        if (dic.ContainsKey(key))
        {
            dic[key] = value;
            return true;
        }
        return false;
    }

    /// <summary>
    /// 딕셔너리 내부에 Key가 없을 경우에만 Value를 추가합니다.
    /// </summary>
    /// <returns>추가에 성공했다면 True를 반환합니다.</returns>
    public static bool TryAdd<K, T>(this Dictionary<K, T> dic, K key, T value)
    {
        if (!dic.ContainsKey(key))
        {
            dic.Add(key, value);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Get Simply TryGet In Dictionary
    /// </summary>
    /// <returns>If TryGet fails, Return the null</returns>
    public static T Get<K, T>(this Dictionary<K, T> dict, K key)
    {
        T obj;
        if (dict.TryGetValue(key, out obj))
        {
            return obj;
        }
        else
        {
            return default(T);
        }
    }

    /// <summary>
    /// Get Simply TryGet In Dictionary
    /// </summary>
    /// <returns>If TryGet fails, Return the value 'def'</returns>
    public static T Get<K, T>(this Dictionary<K, T> dict, K key, T def)
    {
        T obj;
        if (dict.TryGetValue(key, out obj))
        {
            return obj;
        }
        else
        {
            return def;
        }
    }

    /// <summary>
    /// 딕셔너리를 딕셔너리에 추가합니다. src의 타입 상속관계도 허용합니다.
    /// </summary>
    public static void AddRange<K, T, T2>(this Dictionary<K, T> dest, Dictionary<K, T2> src)
        where T : class
        where T2 : T
    {
        IEnumerator<KeyValuePair<K, T2>> e = src.GetEnumerator();
        while (e.MoveNext())
        {
            dest.Add(e.Current.Key, e.Current.Value);
        }
    }

    public static void TryAddRange<K, T, T2>(this Dictionary<K, T> dest, Dictionary<K, T2> src)
        where T : class
        where T2 : T
    {
        IEnumerator<KeyValuePair<K, T2>> e = src.GetEnumerator();
        while (e.MoveNext())
        {
            dest.TryAdd(e.Current.Key, e.Current.Value);
        }
    }

    /// <summary>
    /// 딕셔너리에 딕셔너리를 추가합니다. src의 타입 상속관계도 허용합니다. 또한 Dictionary.Put과 같은 원리로 동작합니다.
    /// </summary>
    public static void PutRange<K, T, T2>(this Dictionary<K, T> dest, Dictionary<K, T2> src)
        where T : class
        where T2 : T
    {
        IEnumerator<KeyValuePair<K, T2>> e = src.GetEnumerator();
        while (e.MoveNext())
        {
            dest.Put(e.Current.Key, e.Current.Value);
        }
    }

    /// <summary>
    /// Get Simply TryGet In Dictionary
    /// </summary>
    /// <returns>If TryGet fails, Return the value 'def'</returns>
    public static T Get<K, T>(this Dictionary<K, T> dict, K key, Func<Dictionary<K, T>, T> def)
    {
        T obj;
        if (dict.TryGetValue(key, out obj))
        {
            return obj;
        }
        else
        {
            return def(dict);
        }
    }
    /// <summary>
    /// Get Simply TryGet In Dictionary
    /// </summary>
    /// <returns>If TryGet fails, Return the value 'def'</returns>
    public static T Get<K, T>(this Dictionary<K, T> dict, K key, Func<T> def)
    {
        T obj;
        if (dict.TryGetValue(key, out obj))
        {
            return obj;
        }
        else
        {
            return def();
        }
    }
    #endregion

    #region Get Parsing
    /// <summary>
    /// 찾으려는 대상이 기본형 타입을 경우에 무조건 해당 타입으로 결과값을 반환합니다.
    /// </summary>
    public static int GetInt<TKey, TVAlue>(this Dictionary<TKey, TVAlue> dic, TKey key, int def = 0)
    {
        try
        {
            object value = dic[key];
            Type type = value.GetType();
            if (type == typeof(int)) return (int)value;
            else if (type == typeof(string)) return int.Parse((string)value);
            else if (type == typeof(float)) return (int)(float)value;
            else if (type == typeof(bool)) return (bool)value ? 1 : 0;
            else if (type == typeof(byte)) return (int)(byte)value;
            else if (type == typeof(short)) return (int)(short)value;
            else if (type == typeof(long)) return (int)(long)value;
            else if (type == typeof(uint)) return (int)(uint)value;
            else if (type == typeof(double)) return (int)(double)value;
            else if (type == typeof(sbyte)) return (int)(sbyte)value;
            else if (type == typeof(ulong)) return (int)(ulong)value;
            else if (type == typeof(decimal)) return (int)(decimal)value;
            return Convert.ToInt32(dic[key]);
        }
        catch { return def; }
    }
    public static float GetFloat<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key, float def = 0)
    {
        try
        {
            object value = dic[key];
            Type type = value.GetType();
            if (type == typeof(float)) return (float)value;
            else if (type == typeof(string)) return float.Parse((string)value);
            else if (type == typeof(int)) return (float)(int)value;
            else if (type == typeof(bool)) return (bool)value ? 1 : 0;
            else if (type == typeof(byte)) return (float)(byte)value;
            else if (type == typeof(short)) return (float)(short)value;
            else if (type == typeof(long)) return (float)(long)value;
            else if (type == typeof(uint)) return (float)(uint)value;
            else if (type == typeof(double)) return (float)(double)value;
            else if (type == typeof(sbyte)) return (float)(sbyte)value;
            else if (type == typeof(ushort)) return (float)(ushort)value;
            else if (type == typeof(ulong)) return (float)(ulong)value;
            else if (type == typeof(decimal)) return (float)(decimal)value;
            return Convert.ToSingle(dic[key]);
        }
        catch { return def; }
    }
    public static double GetDouble<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key, double def = 0)
    {
        try
        {
            object value = dic[key];
            Type type = value.GetType();
            if (type == typeof(double)) return (double)value;
            else if (type == typeof(string)) return double.Parse((string)value);
            else if (type == typeof(int)) return (double)(int)value;
            else if (type == typeof(float)) return (double)(float)value;
            else if (type == typeof(bool)) return (bool)value ? 1 : 0;
            else if (type == typeof(byte)) return (double)(byte)value;
            else if (type == typeof(short)) return (double)(short)value;
            else if (type == typeof(long)) return (double)(long)value;
            else if (type == typeof(uint)) return (double)(uint)value;
            else if (type == typeof(sbyte)) return (double)(sbyte)value;
            else if (type == typeof(ushort)) return (double)(ushort)value;
            else if (type == typeof(ulong)) return (double)(ulong)value;
            else if (type == typeof(decimal)) return (double)(decimal)value;
            return Convert.ToDouble(dic[key]);
        }
        catch { return def; }
    }
    public static bool GetBool<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key, bool def = false)
    {
        try
        {
            object value = dic[key];
            Type type = value.GetType();
            if (type == typeof(bool)) return (bool)value;
            else if (type == typeof(string)) return bool.Parse((string)value);
            else if (type == typeof(int)) return (int)value != 0;
            else if (type == typeof(float)) return (float)value != 0;
            else if (type == typeof(double)) return (double)value != 0;
            else if (type == typeof(byte)) return (byte)value != 0;
            else if (type == typeof(short)) return (short)value != 0;
            else if (type == typeof(long)) return (long)value != 0;
            else if (type == typeof(uint)) return (uint)value != 0;
            else if (type == typeof(sbyte)) return (sbyte)value != 0;
            else if (type == typeof(ushort)) return (ushort)value != 0;
            else if (type == typeof(ulong)) return (ulong)value != 0;
            else if (type == typeof(decimal)) return (decimal)value != 0;
            return Convert.ToBoolean(value);
        }
        catch { return def; }
    }
    public static long GetLong<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key, long def = 0)
    {
        try
        {
            object value = dic[key];
            Type type = value.GetType();
            if (type == typeof(long)) return (long)value;
            else if (type == typeof(string)) return long.Parse((string)value);
            else if (type == typeof(int)) return (long)(int)value;
            else if (type == typeof(float)) return (long)(float)value;
            else if (type == typeof(bool)) return (bool)value ? 1L : 0L;
            else if (type == typeof(byte)) return (long)(byte)value;
            else if (type == typeof(short)) return (long)(short)value;
            else if (type == typeof(double)) return (long)(double)value;
            else if (type == typeof(uint)) return (long)(uint)value;
            else if (type == typeof(sbyte)) return (long)(sbyte)value;
            else if (type == typeof(ushort)) return (long)(ushort)value;
            else if (type == typeof(ulong)) return (long)(ulong)value;
            else if (type == typeof(decimal)) return (long)(decimal)value;
            return Convert.ToInt64(value);
        }
        catch { return def; }
    }
    public static string GetString<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key, string def = null)
    {
        try { return dic[key].ToString(); }
        catch { return def ?? string.Empty; }
    }
    #endregion

    #region Filter
    /// <summary>
    /// 특정 조건에 대해서만 갯수를 계산합니다.
    /// </summary>
    public static int Count<K, T>(this Dictionary<K, T> dic, Func<T, bool> filter)
    {
        if (filter == null) return dic.Count;

        int count = 0;
        var en = dic.GetEnumerator();
        while (en.MoveNext())
        {
            if (filter(en.Current.Value)) count++;
        }
        return count;
    }
    /// <summary>
    /// 특정 조건에 대해서 Enumerator로 획득합니다.
    /// </summary>
    public static IEnumerator<T> GetEnumerator<K, T>(this Dictionary<K, T> dic, Func<T, bool> filter)
    {
        var en = dic.GetEnumerator();
        while (en.MoveNext())
        {
            if (filter(en.Current.Value))
            {
                yield return en.Current.Value;
            }
        }
    }
    #endregion

}
