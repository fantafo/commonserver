﻿using FTF.Server;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FIMS.Common.Server
{
    public partial class ServerUI : Form
    {
        public static ServerUI main;

        public ServerUI()
        {
            main = this;
            InitializeComponent();
        }

        private void ServerUI_Load(object sender, EventArgs e)
        {
            RoomManager.OnNewRoom += OnNewRoom;
            RoomManager.OnRemoveRoom += OnRemoveRoom;

            BeginInvoke(new Action(() =>
            {
                lock (RoomManager.LOCK)
                {
                    for (int i = 0; i < RoomManager.Count; i++)
                    {
                        var room = RoomManager.Get(i);
                        if (room != null && !room.IsStarted)
                        {
                            roomList.Items.Add(room);
                        }
                    }
                }
            }));
        }

        private void ServerUI_FormClosing(object sender, FormClosingEventArgs e)
        {
            RoomManager.OnNewRoom -= OnNewRoom;
            RoomManager.OnRemoveRoom -= OnRemoveRoom;

            var rooms = RoomManager.GetEnumerator();
            while (rooms.MoveNext())
            {
                rooms.Current.OnNewPlayer -= OnNewPlayer;
                rooms.Current.OnRemovePlayer -= OnRemovePlayer;
                rooms.Current.OnMasterPlayer -= OnMasterPlayer;
            }
        }

        private void OnNewRoom(RoomInstance room)
        {
            room.OnNewPlayer += OnNewPlayer;
            room.OnRemovePlayer += OnRemovePlayer;
            room.OnMasterPlayer += OnMasterPlayer;
            BeginInvoke(new Action(() =>
            {
                roomList.Items.Add(room);

                if (roomList.SelectedItem == null)
                {
                    roomList.SelectedItem = room;
                }
            }));
        }

        private void OnRemoveRoom(RoomInstance room)
        {
            BeginInvoke(new Action(() =>
            {
                roomList.Items.Remove(room);
            }));
        }

        private void OnNewPlayer(RoomInstance room, PlayerInstance player)
        {
            BeginInvoke(new Action(() =>
            {
                if (roomList.SelectedItem == room)
                {
                    userList.Items.Add(player);
                }
            }));
        }
        private void OnRemovePlayer(RoomInstance room, PlayerInstance player)
        {
            BeginInvoke(new Action(() =>
            {
                if (roomList.SelectedItem == room)
                {
                    userList.Items.Remove(player);
                }
            }));
        }
        private void OnMasterPlayer(RoomInstance room, PlayerInstance player)
        {
            BeginInvoke(new Action(() =>
            {
                if (roomList.SelectedItem == room)
                {
                    userList.Items.Clear();
                    foreach (var p in room.players)
                    {
                        userList.Items.Add(p);
                    }
                }
            }));
        }


        private void roomDelete(object sender, EventArgs e)
        {
            foreach (var obj in roomList.SelectedItems)
            {
                RoomInstance room = obj as RoomInstance;
                if (room != null)
                {
                    room.ForceStop();
                    RoomManager.Remove(room);
                }
                else
                {
                    roomList.Items.Remove(null);
                }
            }
        }

        private void roomStart(object sender, EventArgs e)
        {
            foreach (var obj in roomList.SelectedItems)
            {
                RoomInstance room = obj as RoomInstance;
                if (room != null)
                {
                    if (!room.IsStarted)
                    {
                        room.ForceStart();
                    }
                }
                else
                {
                    roomList.Items.Remove(null);
                }
            }
        }

        private void changeMaster(object sender, EventArgs e)
        {
            RoomInstance room = roomList.SelectedItem as RoomInstance;
            PlayerInstance player = userList.SelectedItem as PlayerInstance;
            if (room != null && player != null)
            {
                room.ChangeMaster(player);
            }
        }

        private void kickPlayer(object sender, EventArgs e)
        {
            PlayerInstance player = userList.SelectedItem as PlayerInstance;
            if (player != null)
            {
                var client = player.account.client;
                var room = player.room;

                if (client != null)
                {
                    client.Disconnect();
                    client.Close();
                }

                if (room != null)
                {
                    room.RemovePlayer(player);
                    room.BroadcastPlayersInfos();
                }
            }
        }

        private void startAll(object sender, EventArgs e)
        {
            lock (RoomManager.LOCK)
            {
                for (int i = 0; i < RoomManager.Count; i++)
                {
                    var room = RoomManager.Get(i);
                    if (room != null && !room.IsStarted)
                    {
                        room.ForceStart();
                    }
                }
            }
        }

        private void Update(object sender, EventArgs e)
        {
            RoomInstance room = roomList.SelectedItem as RoomInstance;
            if (room != null)
            {

            }
        }

        private void roomList_SelectedIndexChanged(object sender, EventArgs e)
        {
            userList.Items.Clear();
            RoomInstance room = roomList.SelectedItem as RoomInstance;
            if (room != null)
            {
                foreach (var p in room.players)
                {
                    userList.Items.Add(p);
                }
                Update(null, null);
            }
        }

        private void refresh(object sender, EventArgs e)
        {
            roomList.SelectedItem = null;
            roomList.Items.Clear();
            userList.Items.Clear();
            lock (RoomManager.LOCK)
            {
                for (int i = 0; i < RoomManager.Count; i++)
                {
                    var room = RoomManager.Get(i);
                    if (room != null && !room.IsStarted)
                    {
                        roomList.Items.Add(room);
                    }
                }
            }
        }

        static int dummyID = 1;
        private void button7_Click(object sender, EventArgs e)
        {
            var room = roomList.SelectedItem as RoomInstance;
            Account account = new Account
            {
                SN = new Random().Next(int.MinValue, -100000),
                Id = "Dummy",
                Password = "Dummy",
                Name = "Dummy" + (dummyID++),
                AvatarHead = (short)new Random().Next(1, 4),
                AvatarBody = (short)new Random().Next(1, 4),
                client = new Client(),
            };
            DummyPlayerInstance player = new DummyPlayerInstance();
            player.account = account;

            room.JoinPlayer(player);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var room = roomList.SelectedItem as RoomInstance;
            for (int i = 0; i < 10; ++i)
            {
                Account account = new Account
                {
                    SN = new Random().Next(int.MinValue, -100000),
                    Id = "Dummy",
                    Password = "Dummy",
                    Name = "Dummy" + (dummyID++),
                    AvatarHead = (short)new Random().Next(1, 4),
                    AvatarBody = (short)new Random().Next(1, 4),
                    client = new Client(),
                };
                DummyPlayerInstance player = new DummyPlayerInstance();
                player.account = account;

                room.JoinPlayer(player);
            }
        }
    }
}
