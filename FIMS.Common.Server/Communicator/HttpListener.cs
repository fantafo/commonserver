﻿using FTF.Server;
using FTF.Server.Packet;
using Newtonsoft.Json.Linq;
using FTF.http.Anchor;
using FTF.http;
using System.Collections.Generic;
using System.Dynamic;
using System;
using System.Net;
using System.IO;
using System.Text;


namespace FTF
{
    public class HttpListener
    {
        #region Constructor
        const string TAG = "http";

        private static AnchorServer server;
        static HttpListener()
        {
            // Log Setting
            FTF.http.Logger.OnLogDebug += (s) => { SLog.d(TAG, s); };
            FTF.http.Logger.OnLogWarn += (s) => { SLog.w(TAG, s); };
            FTF.http.Logger.OnLogInfo += (s) => { SLog.d(TAG, s); };
            FTF.http.Logger.OnLogError += (s) => { SLog.e(TAG, s); };
        }
        public static void Start()
        {
            if (server == null || !server.IsRun)
            {
                // Anchor Server
                server = new AnchorServer();
                while (true)
                {
                    var availablePort = IpHelper.AvailablePortRange(ServerSetting.HttpPort, ServerSetting.HttpPort + 1000);
                    if (server.TryBind(availablePort))
                    {
                        ServerSetting.HttpPort = availablePort;
                        break;
                    }
                }

                // Add Content
                server.AddContent(new HttpListener());
            }
        }
        public static void Stop()
        {
            if (server != null && server.IsRun)
            {
                server.Stop();
            }
        }
        #endregion Constructor

        public SLog log;

        /// <summary>
        /// http GET
        /// 
        /// 버전정보를 요청한다.
        /// 대부분 서버가 살아있는지 체크할때 단순하게 사용한다.
        /// </summary>
        [Path("/Version")]
        public dynamic Version(HttpListenerContext context)
        {
            return new
            {
                Version = 1
            };
            //return "{Version:1}";
        }

        /// <summary>
        /// http POST
        /// param SN        생성해야할 방번호
        /// param Level     방의 맵
        /// param Name      방제목
        /// param members   방유저 목록
        /// param game      실행할 게임정보
        /// 
        /// 들어온 정보를 토대로 방을 생성합니다.
        /// 게임의 패키지,버전이 기입 돼 있기때문에 특정 게임 패키지,버전에서만 방에 들어올 수 있습니다.
        /// 들어올 유저를 미리 추가해준다.
        /// </summary>
        [Path("/Create")]
        public object Create(HttpListenerContext context)
        {
            // 기본 정보 읽기
            JObject req = context.Request.GetJsonContent();
            long sn = req["SN"].Value<long>();
            short map = req["Map"].Value<short>();  //새롭게 추가.. 이값을 통해 씬을 결정.
            int level = req["Level"].Value<short>();//기존에 level은 씬을 결정. 지구에서는 항상1. 현재는 씬에서 레벨을 결정해줌.
            string detail = req["Detail"].Value<string>();  //360영상의 경우 실행할 파일명을 전달
            string name = req["Name"].Value<string>();
            bool fast = req["Fast"].Value<bool>();  //빠른 시작 여부, true 1번씬 실행, false 0번씬 실행
            bool observer = req["Observer"].Value<bool>();  //옵저버 실행 여부, true 실행, false 실행안함
            JArray members = req["members"] as JArray;
            JObject game = req["game"] as JObject;

            if(!game["PackageName"].Value<string>().Equals("com.fantafo.safety"))
            {
                map = 1;
            }
            string msg = $"Create /Num:{sn} /Name:{name} /Map:{map} /Level:{level} /User:{members.Count} /GameName:{game["PackageName"].Value<string>()} ";
            if (detail.IsEmpty() == false)
                msg += $"Detail:{detail}";
            SLog.d(TAG, msg);

            // sn 검사
            //if(RoomManager.Contains(sn))
            //{
            sn = RoomManager.FindEmptyRoomNumber();
            //}

            // 방생성
            RoomInstance room = new RoomInstance();
            room.IsFixedRoom = true;
            room.Num = sn;
            room.Name = name;
            room.Password = name;
            room.MaxUser = (byte)members.Count;
            room.Level = (short)level;  
            room.MapStage = 0;  //유니티 씬 넘버  //  safety 경우 0임, uacon일경우 level로..
            room.MapID = (short)map;
            room.Detail = detail;   //실행할 영상 정보는 접속시 전달
            room.Fast = fast;
            room.b_Observer = observer;
            room.ClientIdentifier = game["PackageName"].Value<string>();
            string msglog = $"create room {room.ClientIdentifier}, room.MapID {room.MapID}";
            if (room.ClientIdentifier == "com.fantafo.videoplayer")
            {
                room.MapStage = (short)level;
                msglog += $" player video : {room.Detail}";
            }
            SLog.d(TAG, msglog);
            room.ClientVersion = game["Version"].Value<int>();
            RoomManager.Add(room);
            // 유저 임시 추가
            //Random random = new Random();
            for (int i = 0; i < members.Count; i++)
            {
                JObject sessUser = members[i] as JObject;
                JObject dev = sessUser["device"] as JObject;

                Account acc = AccountManager.Login(null, dev["SN"].Value<long>(), dev["Name"].Value<string>());
                if (acc != null && acc.player != null)
                {
                    acc.Name = sessUser["Name"].Value<string>();

                    room.AddPlayer(acc.player);
                }
            }

            return new
            {
                num = room.Num
            };
        }


        /// <summary>
        /// http GET
        /// param num   방번호
        /// 
        /// 해당 방번호의 방의 게임을 시작시킵니다.
        /// </summary>
        [Path("/Start")]
        public object Start(HttpListenerContext context)
        {
            long num = context.Request.GetLongQuery("num");
            //bool observer = context.Request.GetBoolQuery("observer");
            RoomInstance room = RoomManager.GetNum(num);
            string msg = $"Start {num}, Level: {room.Level} observer : {room.b_Observer}";
            SLog.d(TAG, msg);
            if (room != null)
            {
                room.ForceStart(true, room.Level);
                return null;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound, "Not_Found_Room_Number");
            }
        }

        /// <summary>
        /// http GET
        /// param num   방번호
        /// 
        /// 방을 종료합니다.
        /// </summary>
        [Path("/Stop")]
        public object Stop(HttpListenerContext context)
        {
            long num = context.Request.GetLongQuery("num");

            SLog.d(TAG, $"Stop {num}");
            RoomInstance room = RoomManager.GetNum(num);
            if (room != null)
            {
                room.ForceStop(true);
                return null;
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound, "Not_Found_Room_Number");
            }
        }

        /// <summary>
        /// http GET
        /// param num   방번호
        /// 
        /// 방에 관련된 상세정보를 가져옵니다.
        /// </summary>
        [Path("/Info")]
        public object Info(HttpListenerContext context)
        {
            long num = context.Request.GetLongQuery("num");

            //SLog.d(TAG, $"Info {num}");
            RoomInstance room = RoomManager.GetNum(num);
            //room = new RoomInstance();
            //room.players.Add(new PlayerInstance());
            //room.players[0].account = new Account();
            if (room != null)
            {
                List<dynamic> userlist = new List<dynamic>();
                for (int i = 0; i < room.players.Count; i++)
                {
                    var player = room.players[i];
                    if (player != null)
                    {
                        userlist.Add(new
                        {
                            sn = player.account.SN,
                            isConnected = player.IsConnected,
                            isDead = player.isDead,
                            instanceID = player.instanceID,
                            level = player.level,
                            score = player.score
                        });
                    }
                }

                return new
                {
                    isStart = room.IsStarted,
                    level = room.MapID,
                    stage = room.Stage,
                    elapsedTime = room.elapsedTime,
                    users = userlist,
                };
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound, "Not_Found_Room_Number");
            }
        }

        [Path("/ChangeName")]
        public void ChangeName(HttpListenerContext context)
        {
            // 기본 정보 읽기
            JObject req = context.Request.GetJsonContent();
            long sn = req["RoomNum"].Value<long>();
            int deviceSN = req["DeviceSN"].Value<int>();
            string deviceName = req["DeviceName"].Value<string>();
            string name = req["Name"].Value<string>();
            short head = req["Head"].Value<short>();
            short body = req["Body"].Value<short>();
            SLog.d(TAG, $"ChangeName /SN:{sn} /DeviceSN:{deviceName} /Name:{name}");

            RoomInstance room = RoomManager.GetNum(sn);

            int num = room.players.FindIndex(x => x.account.SN.Equals(deviceSN));
            if (num > -1)
            {
                room.players[num].name = name;
                room.players[num].account.AvatarHead = head;
                room.players[num].account.AvatarBody = body;
                room.players[num].account.client.state = Client.ClientState.Channel;
                room.players[num].account.client.Send(S_Common.LoginResult(S_Common.LoginResultType.OK, name));

                //SLog.d(TAG, $"ChangeName /RoomSN:{sn} /DeviceSN:{room.players[num].account.SN} /DeviceName:{deviceName} /Name:{name} /head:{head} /body:{body}");
            }
        }

        [Path("/LoginFailed")]
        public object LoginFailed(HttpListenerContext context)
        {
            // 기본 정보 읽기
            JObject req = context.Request.GetJsonContent();
            long sn = req["RoomNum"].Value<long>();
            int DeviceSN = req["DeviceSN"].Value<int>();
            SLog.d(TAG, $"LoginFailed / RoomNum : {sn}    DeviceSN : {DeviceSN}");

            // sn 검사
            //if(RoomManager.Contains(sn))
            //{
            //}
            RoomInstance room = RoomManager.GetNum(sn);
            if (room != null)
            {
                int num = room.players.FindIndex(x => x.account.SN.Equals(DeviceSN));
                room.players[num].account.client.Send(S_Common.LoginResult(S_Common.LoginResultType.WrongID));
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound, "Not_Found_Room_Number");
            }
            return null;
        }



        [Path("/Reduce")]
        public object Reduce(HttpListenerContext context)
        {
            // 기본 정보 읽기
            JObject req = context.Request.GetJsonContent();
            long sn = req["RoomNum"].Value<long>();
            int DeviceSN = req["DeviceSN"].Value<int>();
            SLog.d(TAG, $"Reduce / RoomNum : {sn}    DeviceSN : {DeviceSN}");

            // sn 검사
            //if(RoomManager.Contains(sn))
            //{
            //}
            RoomInstance room = RoomManager.GetNum(sn);
            if (room != null)
            {

                room.MaxUser--;
                int index = room.players.FindIndex(x => x.account.SN.Equals(DeviceSN));

                if (index >= 0)
                {
                    room.players.RemoveAt(index);
                }
                
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound, "Not_Found_Room_Number");
            }
            return null;
        }
    }
}