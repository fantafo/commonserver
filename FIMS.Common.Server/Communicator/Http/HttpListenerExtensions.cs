﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FTF.http
{
    public static class HttpListenerExtensions
    {
        public static void Send(this HttpListenerResponse res)
        {
            res.ContentLength64 = 0;
        }
        public static void Send(this HttpListenerResponse res, string text, string mime = "text/plain", Encoding encoding = null)
        {
            if (encoding == null)
                encoding = Encoding.UTF8;

            byte[] data = encoding.GetBytes(text);

            res.ContentType = mime;
            res.ContentEncoding = encoding;
            res.ContentLength64 = data.Length;
            res.OutputStream.Write(data, 0, data.Length);
        }

        public static string GetQuery(this HttpListenerRequest req, string key, string def = null)
        {
            return req.QueryString[key];
        }
        public static int GetIntQuery(this HttpListenerRequest req, string key, int def = 0)
        {
            int result;
            if (int.TryParse(req.QueryString[key], out result))
                return result;
            else
                return def;
        }
        public static long GetLongQuery(this HttpListenerRequest req, string key, long def = 0)
        {
            long result;
            if (long.TryParse(req.QueryString[key], out result))
                return result;
            else
                return def;
        }
        public static float GetFloatQuery(this HttpListenerRequest req, string key, float def = 0)
        {
            float result;
            if (float.TryParse(req.QueryString[key], out result))
                return result;
            else
                return def;
        }
        public static bool GetBoolQuery(this HttpListenerRequest req, string key, bool def = false)
        {
            string val = req.QueryString[key];
            if (val == "0")
                return false;
            else if (val == "1")
                return true;
            else
            {
                bool result;
                if (bool.TryParse(req.QueryString[key], out result))
                    return result;
                else
                    return def;
            }
        }

        public static byte[] GetContent(this HttpListenerRequest req)
        {
            if (req.ContentLength64 > 0)
            {
                byte[] data = new byte[req.ContentLength64];
                int offset = 0;
                while (offset < data.Length)
                {
                    int read = req.InputStream.Read(data, offset, data.Length - offset);
                    offset += read;
                }
                return data;
            }
            return new byte[0];
        }
        public static string GetStringContent(this HttpListenerRequest req)
        {
            byte[] data = req.GetContent();
            if (data.Length > 0)
            {
                return req.ContentEncoding.GetString(data);
            }
            return "";
        }
        public static JObject GetJsonContent(this HttpListenerRequest req)
        {
            return JObject.Parse(req.GetStringContent());
        }
    }
}
