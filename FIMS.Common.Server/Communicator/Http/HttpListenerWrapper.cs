﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;

using System.Threading;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

namespace FTF.http
{
    public class HttpListenerWrapper
    {
        /********************************************************************************/
        /*                                                                              */
        /*                                  Variables                                   */
        /*                                                                              */
        /********************************************************************************/
        public event Action<HttpListenerContext> OnReceive;
        public IPEndPoint BindIPEndPoint { get; private set; }
        public bool IsRun { get; private set; }
        public int MinimumThreadPoolCount { get; set; } = 10;
        public int MaximumThreadPoolCount { get; set; } = 100;
        //Socket socket;
        System.Net.HttpListener listener;

        WorkerThreadPool _threadPool;
        internal WorkerThreadPool GetThreadPool()
        {
            if(_threadPool == null)
                _threadPool = new WorkerThreadPool(MinimumThreadPoolCount, MaximumThreadPoolCount);
            return _threadPool;
        }



        /********************************************************************************/
        /*                                                                              */
        /*                                  Operation                                   */
        /*                                                                              */
        /********************************************************************************/
        public bool TryBind(int port)
        {
            try
            {
                Bind(port);
                return true;
            }
            catch
            {
                return false;
            }
        }
        public void Bind(int port)
        {
            try
            {
                IPEndPoint bindPoint = new IPEndPoint(IPAddress.Any, port);

                using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp))
                {
                    socket.Bind(bindPoint);
                }

                listener = new System.Net.HttpListener();
                string prefix = string.Format("http://*:{0}/", port);

                listener.Prefixes.Add(prefix);
                listener.Start();
                listener.BeginGetContext(onAccept, null);
                BindIPEndPoint = bindPoint;
                IsRun = true;
                Logger.Debug("Binded Http Server " + bindPoint);
            }
            catch (Exception e)
            {
                listener.Close();
                throw e;
            }
        }

        public virtual void Stop()
        {
            IsRun = false;
            if (listener != null)
            {
                listener.Close();
            }
        }


        /********************************************************************************/
        /*                                                                              */
        /*                                Listening Scoekts                             */
        /*                                                                              */
        /********************************************************************************/

        private void onAccept(IAsyncResult ar)
        {
            try
            {
                if (ar.IsCompleted)
                {
                    HttpListenerContext context = listener.EndGetContext(ar);
                    GetThreadPool().QueueWorkerEntry(() =>
                    {
                        BroadcastListener(context);
                    });
                }
            }
            catch (Exception e)
            {
                if (IsRun)
                    Logger.Exception(e);
            }
            finally
            {
                if (IsRun)
                    listener.BeginGetContext(onAccept, null);
            }
        }

        /// <summary>
        /// 서버처리가 완료된 Context를 Receive대상들에게 모두 알립니다.
        /// </summary>
        internal void BroadcastListener(HttpListenerContext context)
        {
            if (OnReceive != null)
            {
                OnReceive(context);
            }
        }
    }
}