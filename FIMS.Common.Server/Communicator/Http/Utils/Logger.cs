﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTF.http
{
    public static class Logger
    {
        public static event Action<string> OnLogDebug;
        public static event Action<string> OnLogInfo;
        public static event Action<string> OnLogWarn;
        public static event Action<string> OnLogError;

        internal static void Debug(string message)
        {
            if (OnLogDebug != null)
                OnLogDebug(message);
        }
        internal static void Info(string message)
        {
            if (OnLogDebug != null)
                OnLogDebug(message);
        }
        internal static void Warning(string message)
        {
            if (OnLogWarn != null)
                OnLogWarn(message);
        }
        internal static void Error(string message)
        {
            if (OnLogError != null)
                OnLogError(message);
        }
        internal static void Exception(Exception e)
        {
            Error(e.ToString());
        }
    }
}
