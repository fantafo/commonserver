﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace FTF.http
{
    internal class WorkerThreadPool
    {
        Stack<WorkerThread> pools;
        Queue<Action> runnableEntries;
        int createdThreadCount;
        int minimumThreadCount;
        int maximumThreadCount;

        public int RunningThreadCount => createdThreadCount - pools.Count;


        public WorkerThreadPool(int minimumThreadCount = 10, int maximumThreadCount = 100)
        {
            this.minimumThreadCount = minimumThreadCount;
            this.maximumThreadCount = maximumThreadCount;

            runnableEntries = new Queue<Action>();
            pools = new Stack<WorkerThread>(minimumThreadCount * 2);
            for (int i = 0; i < minimumThreadCount; i++)
            {
                new WorkerThread(this);
                createdThreadCount++;
            }
            while (pools.Count != createdThreadCount)
            {
                Thread.Sleep(10);
            }
        }

        public void QueueWorkerEntry(Action runnable)
        {
            lock (this)
            {
                if (pools.Count > 0)
                {
                    pools.Pop().Start(runnable);
                }
                else if (createdThreadCount < maximumThreadCount)
                {
                    new WorkerThread(this).Start(runnable);
                    createdThreadCount++;
                }
                else
                {
                    runnableEntries.Enqueue(runnable);
                }
            }
        }

        internal void ReleaseThread(WorkerThread item)
        {
            lock (this)
            {
                pools.Push(item);
            }
        }

        internal Action DequeueRunnable()
        {
            lock (this)
            {
                if (runnableEntries.Count > 0)
                {
                    return runnableEntries.Dequeue();
                }
                else
                {
                    return null;
                }
            }
        }


        internal class WorkerThread
        {
            static int IDS = 0;

            private WorkerThreadPool master;
            private Thread thread;
            internal Action runnable;
            internal volatile bool IsRun;

            public WorkerThread(WorkerThreadPool master)
            {
                this.IsRun = true;
                this.master = master;
                this.thread = new Thread(run);
                this.thread.Name = "HttpWorker-" + (++IDS);
                this.thread.Start();
            }

            public bool Start(Action runnable)
            {
                if (this.runnable == null)
                {
                    this.runnable = runnable;
                    lock (this)
                    {
                        Monitor.Pulse(this);
                    }
                    return true;
                }
                return false;
            }

            public void Stop()
            {
                IsRun = false;
            }

            public void run()
            {
                Action current;
                while (IsRun)
                {
                    if (runnable == null)
                    {
                        master.ReleaseThread(this);
                        lock (this)
                        {
                            Monitor.Wait(this, 500);
                        }
                    }

                    current = runnable;
                    while (current != null)
                    {
                        current();
                        runnable = current = master.DequeueRunnable();
                    }
                }
            }

        }
    }
}
