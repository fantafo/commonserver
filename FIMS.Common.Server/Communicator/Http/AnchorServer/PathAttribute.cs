﻿using System;

namespace FTF.http.Anchor
{
    public class PathAttribute : Attribute
    {
        public string path { get; set; }
        public PathAttribute(string path)
        {
            this.path = path;
        }
    }
}