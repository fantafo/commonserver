﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace FTF.http.Anchor
{
    public struct HttpResponseMessage
    {
        public static HttpResponseMessage OK = new HttpResponseMessage(HttpStatusCode.OK);

        internal HttpStatusCode code;
        internal object message;

        public HttpResponseMessage(HttpStatusCode code, object message = null)
        {
            this.code = code;
            this.message = message;
        }
    }
}
