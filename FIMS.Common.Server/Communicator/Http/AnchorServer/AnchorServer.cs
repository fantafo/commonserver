﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;

using System.Threading;
using System.Text.RegularExpressions;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using System.Web.Script.Serialization;
using System.Dynamic;

namespace FTF.http.Anchor
{
    public class AnchorServer : HttpListenerWrapper
    {
        /********************************************************************************/
        /*                                                                              */
        /*                                  Variables                                   */
        /*                                                                              */
        /********************************************************************************/
        List<AnchorContent> anchorContents = new List<AnchorContent>();

        public void AddContent(object obj)
        {
            if (!anchorContents.Contains((a) => a.target == obj))
            {
                anchorContents.Add(new AnchorContent(obj));
            }
        }
        public void RemoveContent(object obj)
        {
            for (int i = 0; i < anchorContents.Count; i++)
            {
                if (anchorContents[i].target == obj)
                {
                    anchorContents.RemoveAt(i);
                    break;
                }
            }
        }


        /********************************************************************************/
        /*                                                                              */
        /*                                    Constructor                               */
        /*                                                                              */
        /********************************************************************************/
        public AnchorServer()
        {
            base.OnReceive += OnReceiveAnchor;
        }


        /********************************************************************************/
        /*                                                                              */
        /*                                Listening Scoekts                             */
        /*                                                                              */
        /********************************************************************************/
        void OnReceiveAnchor(HttpListenerContext context)
        {
            try
            {
                object result = null;
                string path = context.Request.Url.AbsolutePath;
                bool success = false;
                for (int i = 0; i < anchorContents.Count; i++)
                {
                    if (anchorContents[i].Execute(context, path, ref result))
                    {
                        success = true;
                        break;
                    }
                }

                if (success && SendResult(context, result))
                {
                    return;
                }

                context.Response.StatusCode = 404;
                context.Response.Send("<h1>404 Not Found</h1>");
            }
            catch (Exception e)
            {
                try
                {
                    context.Response.StatusCode = 500;
                    context.Response.Send(e.ToString());
                }
                catch { }
                SLog.e("AnchorServer", e);
            }
            finally
            {
                context.Response.Close();
            }
        }

        bool SendResult(HttpListenerContext context, object result)
        {
            if (result == null)
            {
                return true;
            }
            else if (result is string)
            {
                context.Response.Send((string)result);
                return true;
            }
            else if (result is HttpResponseMessage)
            {
                var msg = (HttpResponseMessage)result;
                context.Response.StatusCode = (int)msg.code;

                if (msg.message is string)
                {
                    context.Response.StatusDescription = (string)msg.message;
                    return true;
                }
                else
                {
                    // 결과값 검색의 처음으로 회귀한다.
                    result = msg.message;
                    return SendResult(context, result);
                }
            }
            else
            {
                string[] accepts = context.Request.AcceptTypes;
                foreach (string accept in accepts)
                {
                    switch (accept.ToLower())
                    {
                        case "text/html":
                        case "text/plain":
                        case "application/json":
                        case "*/*":
                            {
                                JavaScriptSerializer serializer = new JavaScriptSerializer();
                                if (result is ExpandoObject)
                                {
                                    serializer.RegisterConverters(new[] { new ExpandoJSONConverter() });
                                }
                                context.Response.Send(serializer.Serialize(result));
                            }
                            return true;
                    }
                }
            }
            return false;
        }


        /********************************************************************************/
        /*                                                                              */
        /*                                  Anchor Content                              */
        /*                                                                              */
        /********************************************************************************/
        internal class AnchorContent
        {
            internal object target;
            Dictionary<string, Action<HttpListenerContext>> voidMapper = new Dictionary<string, Action<HttpListenerContext>>();
            Dictionary<string, Func<HttpListenerContext, object>> returnMapper = new Dictionary<string, Func<HttpListenerContext, object>>();

            internal AnchorContent(object target)
            {
                this.target = target;
                var type = target.GetType();
                var methods = type.GetMethods(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
                foreach (var method in methods)
                {
                    var attrs = method.GetCustomAttributes(typeof(PathAttribute), false);
                    if (attrs != null && attrs.Length > 0)
                    {
                        var attr = attrs[0] as PathAttribute;

                        if (method.ReturnType == typeof(void))
                        {
                            var func = (Action<HttpListenerContext>)Delegate.CreateDelegate(typeof(Action<HttpListenerContext>), target, method);
                            voidMapper.Add(attr.path, func);
                        }
                        else
                        {
                            var func = (Func<HttpListenerContext, object>)Delegate.CreateDelegate(typeof(Func<HttpListenerContext, object>), target, method);
                            returnMapper.Add(attr.path, func);
                        }
                    }
                }
            }

            internal bool Execute(HttpListenerContext context, string path, ref object result)
            {
                var action = voidMapper.Get(path, (Action<HttpListenerContext>)null);
                if (action != null)
                {
                    action(context);
                    result = null;
                    return true;
                }

                var func = returnMapper.Get(path, (Func<HttpListenerContext, object>)null);
                if (func != null)
                {
                    result = func(context);
                    return true;
                }
                return false;
            }
        }
    }
}