﻿using FTF.Server;
using FTF.Server.Packet;
using Newtonsoft.Json.Linq;
using FTF.http.Anchor;
using System.Collections.Generic;
using System.Dynamic;
using System.Threading;
using System;

namespace FTF
{
    public class HttpCommunicator
    {
        static Thread thread;
        public static bool IsRun { get; private set; }
        public static bool IsOnFims { get; private set; }

        public static void Start()
        {
            if (!IsRun)
            {
                IsRun = true;
                thread = new Thread(new HttpCommunicator().run);
                thread.Name = "HttpSender";
                thread.Start();
            }
        }
        public static void Stop()
        {
            if (IsRun)
            {
                IsRun = false;

                if (thread != null)
                    thread.Join();
                thread = null;
            }
        }

        private void run()
        {
            int[] ports = new int[] { 80, 45455 };
            int portIndex = 0;

            while (IsRun)
            {
                ServerSetting.FimsServerPort = ports[portIndex % ports.Length];
                Thread.Sleep(500);
                try
                {
                    string param = $"ip=";
                    param += (ServerSetting.Publish ? IpHelper.PublicIP : IpHelper.InnerIP);
                    param +=$"&port={ServerSetting.ServerPort}&http={ServerSetting.HttpPort}";
                    HttpHelper.GetHTTP($"/api/Game/GameServerSetting?{param}");
                    IsOnFims = true;
                }
                catch (Exception e)
                {
                    if (e is HttpServerException)
                    {
                        portIndex++;
                    }
                    else
                    {
                        IsOnFims = false;
                        SLog.e("htSender", e);
                    }

                }
            }
        }

    }
}