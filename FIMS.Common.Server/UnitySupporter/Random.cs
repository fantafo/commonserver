﻿namespace UnityEngine
{
    public static class Random
    {
        public static System.Random rnd = new System.Random();

        public static float value => (float)rnd.NextDouble();

        public static float Range(float min, float max) => ((max - min) * value) + min;
        public static int Range(int min, int max) => (int)(System.Math.Max((max - min) * (value - 0.0000000000000000000000000001), 0) + min);
    }
}