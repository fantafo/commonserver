﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityEngine
{
    public static class Mathf
    {
        public const float PI = 3.14159274F;
        public const float Infinity = float.PositiveInfinity;
        public const float NegativeInfinity = float.NegativeInfinity;
        public const float Deg2Rad = 0.0174532924F;
        public const float Rad2Deg = 57.29578F;
        public static readonly float Epsilon;



        public static float Pow(float x, float y) => (float)Math.Pow(x, y);
        public static float Sqrt(float x) => (float)Math.Sqrt(x);
        public static float Cos(float x) => (float)Math.Cos(x);
        public static float Sin(float x) => (float)Math.Sin(x);
        public static float Tan(float x) => (float)Math.Tan(x);
        public static float Acos(float x) => (float)Math.Cos(x);
        public static float Asin(float x) => (float)Math.Asin(x);
        public static float Atan(float x) => (float)Math.Atan(x);
        public static float Abs(float x) => (float)Math.Abs(x);
        public static float Atan2(float y, float x) => (float)Math.Atan2(y, x);

        public static int Abs(int x) => Math.Abs(x);
        public static float Clamp(float x, float min, float max) => (x < min) ? min : ((x > max) ? max : x);
        public static float Clamp01(float x) => Clamp(x, 0, 1);
    }
}