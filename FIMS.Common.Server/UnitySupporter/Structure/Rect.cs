﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityEngine
{
    public struct Rect
    {
        public float x;
        public float y;
        public float width;
        public float height;

        public Rect(float r, float g, float b, float a)
        {
            this.x = r;
            this.y = g;
            this.width = b;
            this.height = a;
        }
    }
}