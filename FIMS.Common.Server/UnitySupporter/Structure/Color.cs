﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityEngine
{
    public struct Color
    {
        public static Color white = new Color(1, 1, 1, 1);

        public float r;
        public float g;
        public float b;
        public float a;

        public Color(float r, float g, float b, float a)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }
    }
    public struct Color32
    {
        public static Color32 white = new Color32(255, 255, 255, 255);

        public byte r;
        public byte g;
        public byte b;
        public byte a;
        public Color32(byte r, byte g, byte b, byte a)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
        }

        public static bool operator ==(Color32 a, Color32 b) => a.r == b.r && a.g == b.g && a.b == b.b && a.a == b.a;
        public static bool operator !=(Color32 a, Color32 b) => a.r != b.r || a.g != b.g || a.b != b.b || a.a != b.a;

        public static implicit operator Color32(Color c)
        {
            return new Color32((byte)(c.r * 255), (byte)(c.g * 255), (byte)(c.b * 255), (byte)(c.a * 255));
        }

        public static implicit operator Color(Color32 c)
        {
            const float SCALE = 1f / 255f;
            return new Color(c.r * SCALE, c.g * SCALE, c.b * SCALE, c.a * SCALE);
        }
    }
}