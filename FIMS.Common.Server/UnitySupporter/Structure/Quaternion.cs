﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityEngine
{
    public struct Quaternion
    {
        public static Quaternion identity = new Quaternion(0, 0, 0, 1);

        public float x;
        public float y;
        public float z;
        public float w;

        public Quaternion(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }

        public Vector3 eulerAngles
        {
            get
            {
                float sqw = this.w * this.w;
                float sqx = this.x * this.x;
                float sqy = this.y * this.y;
                float sqz = this.z * this.z;
                float unit = sqx + sqy + sqz + sqw; // if normalised is one, otherwise is correction factor
                float test = this.x * this.w - this.y * this.z;
                Vector3 v = Vector3.zero;

                if (test > 0.4995f * unit)
                { // singularity at north pole
                    v.y = 2f * Mathf.Atan2(this.y, this.x);
                    v.x = Mathf.PI / 2;
                    v.z = 0;
                    return NormalizeAngles(v * Mathf.Rad2Deg);
                }
                if (test < -0.4995f * unit)
                { // singularity at south pole
                    v.y = -2f * Mathf.Atan2(this.y, this.x);
                    v.x = -Mathf.PI / 2;
                    v.z = 0;
                    return NormalizeAngles(v * Mathf.Rad2Deg);
                }
                Quaternion q = new Quaternion(this.w, this.z, this.x, this.y);
                v.x = (float)Math.Atan2(2f * q.x * q.w + 2f * q.y * q.z, 1 - 2f * (q.z * q.z + q.w * q.w));     // Yaw
                v.y = (float)Math.Asin(2f * (q.x * q.z - q.w * q.y));                             // Pitch
                v.z = (float)Math.Atan2(2f * q.x * q.y + 2f * q.z * q.w, 1 - 2f * (q.y * q.y + q.z * q.z));      // Roll
                return NormalizeAngles(v * Mathf.Rad2Deg);
            }
        }
        Vector3 NormalizeAngles(Vector3 angles)
        {
            angles.x = NormalizeAngle(angles.x);
            angles.y = NormalizeAngle(angles.y);
            angles.z = NormalizeAngle(angles.z);
            return angles;
        }
        float NormalizeAngle(float angle)
        {
            while (angle > 360)
                angle -= 360;
            while (angle < 0)
                angle += 360;
            return angle;
        }

        public override string ToString()
        {
            return $"Quaternion({x:f3},{y:f3},{z:f3},{w:f3})";
        }

        public static Quaternion Euler(Vector3 euler)
        {
            return Euler(euler.x, euler.y, euler.z);
        }
        public static Quaternion Euler(float x, float y, float z)
        {
            x *= Mathf.Deg2Rad;
            y *= Mathf.Deg2Rad;
            z *= Mathf.Deg2Rad;

            float rollOver2 = z * 0.5f;
            float sinRollOver2 = Mathf.Sin(rollOver2);
            float cosRollOver2 = Mathf.Cos(rollOver2);

            float pitchOver2 = y * 0.5f;
            float sinPitchOver2 = Mathf.Sin(pitchOver2);
            float cosPitchOver2 = Mathf.Cos(pitchOver2);

            float yawOver2 = x * 0.5f;
            float sinYawOver2 = Mathf.Sin(yawOver2);
            float cosYawOver2 = Mathf.Cos(yawOver2);

            return new Quaternion(
                cosYawOver2 * sinPitchOver2 * cosRollOver2 + sinYawOver2 * cosPitchOver2 * sinRollOver2,
                sinYawOver2 * cosPitchOver2 * cosRollOver2 - cosYawOver2 * sinPitchOver2 * sinRollOver2,
                cosYawOver2 * cosPitchOver2 * sinRollOver2 - sinYawOver2 * sinPitchOver2 * cosRollOver2,
                cosYawOver2 * cosPitchOver2 * cosRollOver2 + sinYawOver2 * sinPitchOver2 * sinRollOver2
            );
        }
    }
}