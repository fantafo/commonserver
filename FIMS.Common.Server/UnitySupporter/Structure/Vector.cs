﻿using System;
using System.Collections.Generic;
using System.Text;

namespace UnityEngine
{
    public struct Vector2
    {
        public float x;
        public float y;
        public Vector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return $"Vector2({x:f2},{y:f2})";
        }
    }
    public struct Vector3
    {
        public static Vector3 zero = new Vector3(0, 0, 0);
        public static Vector3 one = new Vector3(1, 1, 1);
        public static Vector3 left = new Vector3(-1, 0, 0);
        public static Vector3 right = new Vector3(1, 0, 0);
        public static Vector3 up = new Vector3(0, 1, 0);
        public static Vector3 down = new Vector3(0, -1, 0);
        public static Vector3 forward = new Vector3(0, 0, 1);
        public static Vector3 back = new Vector3(0, 0, -1);

        public float x;
        public float y;
        public float z;

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public override string ToString()
        {
            return $"Vector3({x:f2},{y:f2},{z:f2})";
        }

        public string ToString(int size)
        {
            return string.Format("Vector3({0},{1},{2})", x.ToString("f"+size), y.ToString("f"+size), z.ToString("f"+size));
        }

        public static Vector3 operator +(Vector3 a, Vector3 b) { a.x += b.x; a.y += b.y; a.z += b.z; return a; }
        public static Vector3 operator -(Vector3 a) { a.x *= -1; a.y *= -1; a.z *= -1; return a; }
        public static Vector3 operator -(Vector3 a, Vector3 b) { a.x -= b.x; a.y -= b.y; a.z -= b.z; return a; }
        public static Vector3 operator *(Vector3 a, float d) { a.x *= d; a.y *= d; a.z *= d; return a; }
        public static Vector3 operator *(float d, Vector3 a) { a.x *= d; a.y *= d; a.z *= d; return a; }
        public static Vector3 operator /(Vector3 a, float d) { a.x /= d; a.y /= d; a.z /= d; return a; }
        public static bool operator ==(Vector3 lhs, Vector3 rhs) => lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
        public static bool operator !=(Vector3 lhs, Vector3 rhs) => lhs.x != rhs.x || lhs.y != rhs.y || lhs.z != rhs.z;

    }
    public struct Vector4
    {
        public float x;
        public float y;
        public float z;
        public float w;
        public Vector4(float x, float y, float z, float w)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.w = w;
        }
        public override string ToString()
        {
            return $"Vector3({x:f2},{y:f2},{z:f2},{w:f2})";
        }
    }
}