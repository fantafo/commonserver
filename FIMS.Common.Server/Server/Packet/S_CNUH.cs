﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTF.Server.Packet
{
    class S_CNUH
    {
        public static byte[] MapStageStart(short index = 0)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.CNUH_VideoStart);
            w.WriteH(index);
            return w.ToBytes();
        }
    }
}
