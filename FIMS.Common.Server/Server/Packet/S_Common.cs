﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FTF.Server.Packet
{
    public class S_Common
    {
        public static byte[] Version()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.C_ServerVersion);
            w.WriteD(ServerSetting.Version);
            w.WriteD(ServerSetting.AppReceiveTimeout);
            w.WriteD(ServerSetting.AppSendTimeout);

            return w.ToBytes();
        }

        public enum MessageType : byte
        {
            Alert,
            Warning,
            Error,
        }
        public static byte[] Message(MessageType type, string message)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.C_SystemMessage);
            w.WriteC((byte)type);
            w.WriteS(message);

            return w.ToBytes();
        }

        public enum LoginResultType : byte
        {
            OK,
            WrongID,
            WrongPassword,
        }
        public static byte[] LoginResult(LoginResultType result)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.C_LoginResult);
            w.WriteC((byte)result);
            return w.ToBytes();
        }

        public static byte[] LoginResult(LoginResultType result,string name)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.C_LoginResult);
            w.WriteC((byte)result);
            w.WriteS(name);
            return w.ToBytes();
        }

        public static byte[] CharacterInfo(PlayerInstance player)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.C_UserInfo);
            w.WriteD(player.instanceID);

            w.WriteS(player.name);
            w.WriteH(player.account.Level);
            w.WriteD(player.account.Exp);

            w.WriteH(player.currentHP);
            w.WriteH(player.maxHP);
            w.WriteD(player.score);
            w.WriteH(player.ranking);

            w.WriteB(player.IsReady);
            w.WriteB(player.IsConnected);

            w.WriteL(player.room != null ? player.room.Num : 0);
            if(player.isObserver)
                w.WriteC(-1);
            else
                w.WriteC(player.room != null ? player.room.players.IndexOf(player) : 0);

            w.WriteH(player.headType);
            w.WriteH(player.bodyType);

            w.WriteS(player.VoIP_Address);
            w.WriteH(player.VoIP_Port);

            return w.ToBytes();
        }
        

        public enum DisconnectType
        {
            Unkown,
            Message
        }

        public static byte[] Disconnect(DisconnectType type)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.C_Disconnect);
            w.WriteC((byte)type);
            w.WriteC(0); //string을 감추기위함
            return w.ToBytes();
        }

        public static byte[] Disconnect(string message)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.C_Disconnect);
            w.WriteC((byte)DisconnectType.Message);
            w.WriteS(message);

            return w.ToBytes();
        }

        public static byte[] Ping()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.C_Ping);
            return w.ToBytes();
        }

        public static byte[] Command(List<byte[]> datas)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.C_Command);
            w.WriteC(datas.Count);
            for(int i=0; i<datas.Count; i++)
            {
                w.WriteH(datas[i].Length);
                w.Write(datas[i]);
            }
            return w.ToBytes();
        }

        public static byte[] Command(BinaryReader r)
        {
            int count = r.ReadC();

            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.C_Command);
            w.WriteC(count);
            for (int i = 0; i < count; i++)
            {
                int len = r.ReadH();
                byte[] data = BytePool.Take(len);
                try
                {
                    r.Read(data, 0, len);

                    w.WriteH(len);
                    w.Write(data, 0, len);
                }
                finally
                {
                    BytePool.Release(data);
                }
            }

            return w.ToBytes();
        }
    }
}
