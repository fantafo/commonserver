﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FTF.Server.Packet
{
    class S_Video
    {
        public static byte[] PlayReady(String name)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Video_PlayReady);
            w.WriteS(name);

            return w.ToBytes();
        }

        public static byte[] Info(double ms)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Video_Info);
            w.WriteF((float)ms);
            w.WriteD(ServerSetting.VideoSynchronizationTime);
            return w.ToBytes();
        }

        public static byte[] MapStageStart(short index = 0)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Video_VideoStart);
            w.WriteH(index);
            return w.ToBytes();
        }
    }
}
