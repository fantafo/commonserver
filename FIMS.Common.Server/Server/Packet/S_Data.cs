﻿using System.Collections.Generic;
using UnityEngine;

namespace FTF.Server.Packet
{
    public class S_Data
    {
        public enum TYPE
        {
            String,
            Int,
            Float,
            Bool,
            Vector3,
            Color,
            
            Remove,
            Clear
        }

        ///////////////////////////////////////////////////////////////////
        /// 
        /// 
        /// 
        ///////////////////////////////////////////////////////////////////

        public static byte[] ObjectPackage(Dictionary<string, object> dic)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Scene_SetData);

            w.WriteC(dic.Count);
            var e = dic.GetEnumerator();
            while (e.MoveNext())
            {
                if (e.Current.Value is string)
                {
                    w.WriteC((int)TYPE.String);
                    w.WriteS(e.Current.Key);
                    w.WriteS((string)e.Current.Value);
                }
                else if (e.Current.Value is int)
                {
                    w.WriteC((int)TYPE.Int);
                    w.WriteS(e.Current.Key);
                    w.WriteD((int)e.Current.Value);
                }
                else if (e.Current.Value is float)
                {
                    w.WriteC((int)TYPE.Float);
                    w.WriteS(e.Current.Key);
                    w.WriteF((float)e.Current.Value);
                }
                else if (e.Current.Value is bool)
                {
                    w.WriteC((int)TYPE.Float);
                    w.WriteS(e.Current.Key);
                    w.WriteB((bool)e.Current.Value);
                }
                else if (e.Current.Value is Vector3)
                {
                    w.WriteC((int)TYPE.Vector3);
                    w.WriteS(e.Current.Key);
                    w.WriteV3((Vector3)e.Current.Value);
                }
                else if (e.Current.Value is Color)
                {
                    w.WriteC((int)TYPE.Vector3);
                    w.WriteS(e.Current.Key);
                    w.WriteRGBA((Color)e.Current.Value);
                }
            }
            return w.ToBytes();
        }
    }
}
