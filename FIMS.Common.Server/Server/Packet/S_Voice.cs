﻿using System.Collections.Generic;

namespace FTF.Server.Packet
{
    public class S_Voice
    {
        public static byte[] Data(int instanceID, byte[] data)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Voice_Data);
            w.WriteD(instanceID);
            w.WriteH(data.Length);
            w.Write(data);
            return w.ToBytes();
        }
    }
}
