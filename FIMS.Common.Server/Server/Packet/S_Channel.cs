﻿namespace FTF.Server.Packet
{
    public class S_Channel
    {
        /// <summary>
        /// 채널 목록을 전송해준다.
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public static byte[] RoomList(string identifier)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Room_List);
            lock (RoomManager.LOCK)
            {
                int i = 0, count = 0;
                var e = RoomManager.GetEnumerator();
                while (e.MoveNext())
                    if (e.Current.ClientIdentifier == identifier)
                        count++;

                // write Size
                w.WriteD(count);

                // Write data
                e = RoomManager.GetEnumerator();
                while(e.MoveNext())
                {
                    RoomInstance room = e.Current;
                    if (room.ClientIdentifier == identifier && i++ < count)
                    {
                        w.WriteL(room.Num);
                        w.WriteS(room.Name);
                        w.WriteH(room.MapID);
                        w.WriteC((byte)room.players.Count);
                        w.WriteC(room.MaxUser);
                        w.WriteB(room.IsStarted);
                    }
                }
            }

            return w.ToBytes();
        }
    }
}
