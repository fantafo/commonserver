﻿namespace FTF.Server.Packet
{
    public class S_Brain
    {
        public static byte[] StartIndex(short index)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Brain_Index);
            w.WriteH(index);
            return w.ToBytes();
        }
    }
}
