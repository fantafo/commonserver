﻿using System;

namespace FTF.Server.Packet
{
    public class S_Room
    {
        public static byte[] Info(RoomInstance room)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Room_Info);
            w.WriteL(room.Num);
            w.WriteS(room.Name);
            w.WriteC(room.MaxUser);
            w.WriteH(room.MapID);
            w.WriteH(room.MapStage);
            w.WriteC(room.players.Count);
            for (int i = 0; i < room.players.Count; i++)
            {
                var player = room.players[i];
                w.WriteB(player.IsConnected);
                if (player.IsConnected)
                {
                    w.WriteD(player.instanceID);
                    w.WriteNormal(player.headPosition);
                    w.WriteEuler32(player.headRotation);
                    w.WriteB(player.useHand);
                    w.WriteH(player.ranking);
                    w.WriteH(player.quiz);
                    //SLog.d(player.name + " 순위는 " + player.ranking + "\n");
                    if (player.useHand)
                    {
                        w.WriteNormal(player.handPosition);
                        w.WriteEuler24(player.handRotation);

                        w.WriteB(player.useReticle);
                        if(player.useReticle)
                        {
                            w.WriteV3(player.reticlePosition);
                        }
                    }
                }
            }

            return w.ToBytes();
        }

        public static byte[] Exit()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Room_Exit);
            return w.ToBytes();
        }

        public static byte[] LoadStart(short level = 1)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Room_LoadStart);
            w.WriteH(level);
            SLog.d($"Room_LoadStart level: {level}");
            return w.ToBytes();
        }

        public static byte[] Ban(int instanceID)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Room_LoadStart);
            w.WriteD(instanceID);

            return w.ToBytes();
        }
    }
}
