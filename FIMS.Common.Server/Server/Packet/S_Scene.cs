﻿using System.Collections.Generic;

namespace FTF.Server.Packet
{
    public class S_Scene
    {
        public static byte[] Start()
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Scene_Start);

            return w.ToBytes();
        }
        public static byte[] InitializeTo(int targetInstanceID)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Scene_InitializeTo);
            w.WriteC(1);
            w.WriteD(targetInstanceID);
            return w.ToBytes();
        }
        public static byte[] InitializeTo(IList<int> ids)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Scene_InitializeTo);
            w.WriteC(ids.Count);
            for (int i = 0; i < ids.Count; i++)
            {
                w.WriteD(ids[i]);
            }
            return w.ToBytes();
        }
        public static byte[] End(bool stopApp = true)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Scene_End);
            w.WriteB(stopApp);
            return w.ToBytes();
        }

        public static byte[] SetupCommanderID(string path, string type, uint id)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Scene_SetupCommanderID);
            w.WriteS(path);
            w.WriteS(type);
            w.Write(id);
            return w.ToBytes();
        }

    }
}
