﻿namespace FTF.Server.Packet
{
    public class S_Safety
    {
        public static byte[] NextQuiz(short index)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Safety_Next_Quiz);
            w.WriteH(index);
            return w.ToBytes();
        }

        public static byte[] DetailMapStage(short index)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Safety_DetailMapStage);
            w.WriteH(index);
            return w.ToBytes();
        }

        public static byte[] MapStageStart(short index)
        {
            var w = ThreadBaseBinaryWriter.GetWriter();
            w.WriteO(ServerOpcode.Safety_MapStage_Start);
            w.WriteH(index);
            return w.ToBytes();
        }
    }
}
