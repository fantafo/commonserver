﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEngine;
using FTF.Server.Packet;
using System.Threading;
using System.Threading.Tasks;

namespace FTF.Server
{

    public class PacketHandler
    {
        private Client client;

        private SLog log;
        public PacketHandler(Client client)
        {
            this.client = client;
            log = new SLog("Test");
        }
        /*
        public static DateTime Delay(int MS)
        {
            DateTime ThisMoment = DateTime.Now;
            TimeSpan duration = new TimeSpan(0, 0, 0, 0, MS);
            DateTime AfterWards = ThisMoment.Add(duration);

            while (AfterWards >= ThisMoment)
            {
                System.Windows.Forms.Application.DoEvents();
                ThisMoment = DateTime.Now;
            }

            return DateTime.Now;
        }
        */
        public void MapStageChange(int index, int time)
        {
            Thread.Sleep(time);
            PlayerInstance player = client.player;
            if (player == null)
            {
                client.Disconnect("당신은 아직 로그인하지 않았습니다.");
                return;
            }

            // Check entry in the Room
            RoomInstance room = player.room;
            if (room == null || (!room.players.Contains(player) && !room.observers.Contains(player)))
            {
                client.Disconnect("당신이 속해있는 방을 찾지 못했습니다.");
                return;
            }

            room.MapStage = (short)(room.MapStage > index ? room.MapStage : index);
        }
        public void Handling(ClientOpcode code, BinaryReader r)
        {
            switch (code)
            {
                case ClientOpcode.C_ClientVersion:
                case ClientOpcode.C_Ping:
                case ClientOpcode.C_Login:
                case ClientOpcode.C_Quit:
                    {
                        OnSystemPacket(code, r);
                    }
                    break;

                case ClientOpcode.C_Request_UserInfo:
                case ClientOpcode.Voice_Initialized:
                case ClientOpcode.C_TrackPosition:
                case ClientOpcode.C_VoIPSetup:
                case ClientOpcode.C_Command:
                    {
                        OnUserPacket(code, r);
                    }
                    break;

                case ClientOpcode.Channel_RequestRoomList:
                case ClientOpcode.Channel_CreateRoom:
                case ClientOpcode.Channel_JoinRoom:
                    {
                        OnChannelPacket(code, r);
                    }
                    break;

                case ClientOpcode.Quiz_OX:
                    {
                        OnQuizPacket(code, r);
                    }
                    break;

                case ClientOpcode.Room_Exit:
                case ClientOpcode.Room_Ban:
                case ClientOpcode.Room_ChangeLevel:
                case ClientOpcode.Room_Ready:
                case ClientOpcode.Room_LoadComplete:
                case ClientOpcode.Room_MapStage:
                case ClientOpcode.Room_End:
                    {
                        OnRoomPacket(code, r);
                    }
                    break;
                //case ClientOpcode.Scene_Initialized:
                case ClientOpcode.Scene_UpdateScore:
                case ClientOpcode.Scene_SetData:
                case ClientOpcode.Scene_RequestCommanderID:
                    {
                        OnScenePacket(code, r);
                    }
                    break;

                //satey패킷
                case ClientOpcode.Safety_Contents_End:
                case ClientOpcode.Safety_Quiz_End:
                case ClientOpcode.Safety_Request_DetailMapStage:
                case ClientOpcode.Safety_Start:
                    {
                        OnSafetyPacket(code, r);
                        break;
                    }
                case ClientOpcode.Brain_Index:
                    {
                        OnBrainPacket(code, r);
                        break;
                    }

                //CNUH 패킷
                case ClientOpcode.CNUH_Ready:
                    {
                        OnCNUHPacket(code, r);
                        break;
                    }
                //video 패킷
                case ClientOpcode.Video_Ready:
                    {
                        OnVideoPacket(code, r);
                        break;
                    }
                case ClientOpcode.Quit:
                    {
                        client.Log($"Request Quit");
                        client.Close();
                        break;
                    }
            }
        }

        void OnSystemPacket(ClientOpcode code, BinaryReader r)
        {
            switch (code)
            {
                case ClientOpcode.C_ClientVersion:
                    {
                        string clientIdentifier = r.ReadS();
                        int clientVersion = r.ReadD();
                        int commonPacketVersion = r.ReadD();
                        client._clientType = (r.ReadB() ? Client.ClientType.Editor : 0) | (r.ReadB() ? Client.ClientType.Dummy : 0);

                        client.Log($"Version Server[Cli:{commonPacketVersion} == Serv:{ServerSetting.Version}] Client[{clientVersion}]");

                        if (commonPacketVersion == ServerSetting.Version)
                        {
                            client.identifier = clientIdentifier;
                            client.version = clientVersion;
                            client.Send(S_Common.Version());
                        }
                        else
                        {
                            client.Disconnect("클라이언트 버전이 맞지 않습니다. 관리자에게 문의해주세요.");
                        }
                    }
                    break;
                case ClientOpcode.C_Ping:
                    {
                        var span = (DateTime.Now - client._pingTime);
                        client.pingCheckTime = (int)span.TotalMilliseconds;
                        client.pingCheckCount++;
                        //client.Log($"Ping ({span.TotalMilliseconds:0}ms)");
                    }
                    break;
                case ClientOpcode.C_Login:
                    {
                        bool logined = false;
                        byte type = r.ReadC();
                        // 로그인 타입
                        switch (type)
                        {
                            case 0: // ID, PW를 통한 로그인
                                {
                                    // TODO 구현
                                    string id = r.ReadS();
                                    string pw = r.ReadS();
                                    logined = (AccountManager.Login(client, id, pw) != null);
                                    client.Log($"TryLogin ID:{id}/{pw}");
                                }
                                break;
                            case 10:    //빠른시작
                                {
                                    long deviceSN = r.ReadL();
                                    client.Log($"Try FastLogin Deivce: {deviceSN}");
                                    string msg = $"deviceSN={deviceSN}";
                                    if(AccountManager.Create(client, deviceSN) != null)
                                    {
                                        client.RenameLog();
                                    }

                                    //서버로 비회원 닉네임을 알아온다
                                    HttpHelper.GetHTTP($"/api/Member/FastLogin?{msg}");
                                }
                                break;
                            case 20:    //실제 기기에서 로그인 이용시 학생 id 입력
                                {
                                    long deviceSN = r.ReadL();
                                    string id = r.ReadS();
                                    if (AccountManager.Create(client, deviceSN) != null)
                                    {
                                        client.RenameLog();
                                    }
                                    client.Log($"Try Login {id}");
                                    string msg = $"deviceSN={deviceSN}&id={id}";

                                    //서버로 회원 정보를 확인한다
                                    HttpHelper.GetHTTP($"/api/Member/IDLogin?{msg}");
                                }
                                break;
                            case 100:   //로그아웃
                                {
                                    //Login 패킷에서 로그아웃을 처리?? 
                                    //C_Common에 패킷을 추가해야되는데 지구, 뇌의구조 다 수정해야함...
                                    //나중에 패킷 그룹별로 나눠서 Common=0 Room=10 이렇게 변경 예정
                                    long deviceSN = r.ReadL();
                                    client.Log($"Try LogOut Device SN:  {deviceSN}");
                                    string msg = $"deviceSN={deviceSN}";
                                    
                                    //서버로 회원 로그아웃 전송.
                                    HttpHelper.GetHTTP($"/api/Member/Logout?{msg}");
                                }
                                break;
                            case 240: // SN을 통한 로그인, 현재 사용중..
                                {
                                    long sn = r.ReadL();
                                    logined = (AccountManager.Login(client, sn) != null);
                                    client.Log($"TryLogin SN:{sn}");
                                    
                                    string msg = $"deviceSN={sn}";
                                    HttpHelper.GetHTTP($"/api/Member/FastLogin?{msg}");
                                }
                                break;
                            case 245: // 임시 계정생성하면서 로그인
                                {
                                    string id = r.ReadS();
                                    client.Log($"TryLogin ID:{id}");
                                    Account acc = AccountManager.Login(client, id, id);
                                    logined = (acc != null);

                                    if (logined)
                                    {
                                        acc.SN = UnityEngine.Random.Range(int.MinValue, -1);
                                    }
                                }
                                break;
                            case 255:
                                {
                                    // TODO 구현
                                    Account acc = AccountManager.LoginObserver(client);
                                    acc.player.isObserver = true;
                                    logined = true;

                                }
                                break;
                        }

                        // 로그인 시도
                        if (logined)
                        {
                            // 최초 플레이어정보는 자신의 정보이기에
                            // 신속히 보내줍시다!
                            if (client.player != null)
                            {
                                client.Send(client.player.PacketInfo());
                            }
                            client.RenameLog();
                            client.Log($"Success");
                            client.state = Client.ClientState.Channel;
                            
                        }
                        else
                        {
                            if(type != 10 && type != 20 && type != 100)
                                client.Log($"Failed");
                        }
                    }
                    break;

                case ClientOpcode.C_Quit:
                    {
                        client.Log($"Request Quit");
                        client.Close();
                    }
                    break;
            }
        }

        void OnUserPacket(ClientOpcode code, BinaryReader r)
        {
            switch (code)
            {
                case ClientOpcode.C_Request_UserInfo:
                    {
                        int instanceID = r.ReadD();
                        PlayerInstance player = null;

                        // 자신에 대한 정보를 요청 (instanceID = 0)
                        if (instanceID == 0)
                        {
                            if (client.player == null)
                            {
                                AccountManager.LoadPlayerInstance(client.account);
                            }
                            player = client.player;
                        }
                        else
                        {
                            player = PlayerManager.GetId(instanceID);
                        }

                        // 정보 전송
                        if (player != null)
                        {
                            client.Send(player.PacketInfo());
                        }
                        else
                        {
                            client.LogError($"Not found user Info : {instanceID}");
                            client.Msg("유저 정보를 찾을 수 없습니다. : " + instanceID);
                        }
                    }
                    break;

                case ClientOpcode.Voice_Initialized:
                    {
                        if (client.player != null)
                        {
                            client.player.Voice_Frequency = r.ReadD();
                            client.player.Voice_Channel = r.ReadC();
                            client.player.Voice_FrameSample = r.ReadD();

                            if (client.player.room != null)
                            {
                                client.player.Broadcast(client.player.PacketInfo());
                            }
                        }
                    }
                    break;

                case ClientOpcode.C_TrackPosition:
                    {
                        if (client.player != null)
                        {
                            client.player.headPosition = r.ReadNormal();
                            client.player.headRotation = r.ReadEuler32();
                            client.player.useHand = r.ReadBoolean();

                            if (client.player.useHand)
                            {
                                client.player.handPosition = r.ReadNormal();
                                client.player.handRotation = r.ReadEuler24();
                                client.player.useReticle = r.ReadB();

                                if (client.player.useReticle)
                                {
                                    client.player.reticlePosition = r.ReadVector3();
                                }
                            }
                        }
                        else
                        {
                            client.LogWarning("Not initialized player. but still client sent Track Position");
                        }
                    }
                    break;

                case ClientOpcode.C_VoIPSetup:
                    {

                    }
                    break;

                case ClientOpcode.C_Command:
                    {
                        byte[] data = S_Common.Command(r);

                        byte playerCount = r.ReadC();
                        if (playerCount == byte.MaxValue)
                        {
                            // 전체에게 전송
                            //client.Log($"MultiCommand:Broadcast");
                            client.player.Broadcast(data, null);
                        }
                        else if (playerCount == byte.MinValue)
                        {
                            // 나를 제외하고 전송
                            client.player.Broadcast(data);
                        }
                        else
                        {
                            // 각각 플레이어에게 전송
                            //client.Log($"MultiCommand");
                            for (int i = 0; i < playerCount; i++)
                            {
                                int instanceID = r.ReadD();
                                PlayerInstance target = PlayerManager.GetId(instanceID);
                                if (target != null)
                                {
                                    target.Send(data);
                                }
                            }
                        }
                    }
                    break;
            }
        }

        /// <summary>
        /// 게임에 로그인한 뒤, 방에 들어가지 않은상태에서의 패킷들의 처리.
        /// 방 목록 불러오기, 새로운 방 만들기, 방 들어가기 등의 패킷
        /// </summary>
        /// <param name="code"></param>
        /// <param name="r"></param>
        void OnChannelPacket(ClientOpcode code, BinaryReader r)
        {
            PlayerInstance player = client.player;
            if (client.player == null)
            {
                client.LogError("Not Signed Player. but client sent channel packet.");
                client.Disconnect("당신은 아직 로그인하지 않았습니다.");
                return;
            }

            switch (code)
            {
                case ClientOpcode.Channel_RequestRoomList:
                    {
                        client.Log("RequestList");
                        client.Send(S_Channel.RoomList(client.identifier));
                    }
                    break;
                case ClientOpcode.Channel_CreateRoom:
                    {
                        string roomName = r.ReadS();
                        string password = r.ReadS();
                        short mapID = r.ReadH();
                        byte maxUser = r.ReadC();
                        bool autoStart = r.ReadB();

                        client.Log($"CreateRoom {roomName}/{password}/{mapID}/{maxUser}/{autoStart}");

                        lock (RoomManager.LOCK)
                        {
                            RoomInstance room = new RoomInstance
                            {
                                Num = RoomManager.FindEmptyRoomNumber(),
                                Name = roomName,
                                Password = password,
                                MapID = mapID,
                                MaxUser = maxUser,
                                ClientIdentifier = client.identifier,
                                ClientVersion = client.version,
                                IsStarted = autoStart
                            };

                            RoomManager.Add(room);
                            room.JoinPlayer(player);
                        }
                    }
                    break;
                case ClientOpcode.Channel_JoinRoom:
                    {
                        // Search Room
                        long roomNo = r.ReadL();
                        string password = r.ReadS();
                        client.Log($"JoinRoom {roomNo}/{password}");

                        lock (RoomManager.LOCK)
                        {
                            RoomInstance room = RoomManager.GetNum(roomNo);
                            if (room == null)
                            {
                                client.Log($"  -Create");
                                // 룸이 없을경우 자동생성
                                RoomManager.Add(room = new RoomInstance
                                {
                                    Num = roomNo,
                                    Name = password,
                                    Password = password,
                                    MapID = 1,
                                    ClientIdentifier = client.identifier,
                                    ClientVersion = client.version
                                });
                            }

                            lock (room)
                            {
                                client.Log($"JoinRoom Num:{roomNo} | Ps:{password}/{room.Password} | CI:{client.identifier}/{room.ClientIdentifier} | CV:{client.version}/{room.ClientVersion}");
                                if (!room.Password.IsEmpty() && password != "OBSERVERPASSWORD" && room.Password != password)
                                {
                                    client.Msg("방 접속 비밀번호가 틀렸습니다.");
                                }
                                else if (room.players.Count == 0 || (room.ClientIdentifier == client.identifier && room.ClientVersion == client.version))
                                {
                                    if (!room.JoinPlayer(player))
                                    {
                                        client.Disconnect("방에 자리가 남아있지 않습니다.");
                                    }
                                }
                                else
                                {
                                    client.Disconnect("클라이언트 혹은 버전이 맞지 않습니다.");
                                }
                            }
                        }
                    }
                    break;
                default:
                    client.LogWarningFormat("Unkown Opcode '{0}({1})'", code.ToString(), (int)code);
                    break;
            }
        }

        /// <summary>
        /// 게임에 로그인한 뒤, 방에 들어가지 않은상태에서의 패킷들의 처리.
        /// 방 목록 불러오기, 새로운 방 만들기, 방 들어가기 등의 패킷
        /// </summary>
        /// <param name="code"></param>
        /// <param name="r"></param>
        void OnQuizPacket(ClientOpcode code, BinaryReader r)
        {
            switch (code)
            {
                case ClientOpcode.Quiz_OX:
                    {
                        if (client.player != null)
                        {
                            client.player.quiz = r.ReadH();
                        }
                        else
                        {
                            client.LogWarning("Not initialized player. but still client sent Quiz_OX");
                        }
                    }
                    break;
                default:
                    client.LogWarningFormat("Unkown Opcode '{0}({1})'", code.ToString(), (int)code);
                    break;
            }
        }


        /// <summary>
        /// 방에 들어간 뒤의 패킷 처리.
        /// 방장 권한, 방 나가기, 게임 시작 등.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="r"></param>
        void OnRoomPacket(ClientOpcode code, BinaryReader r)
        {
            PlayerInstance player = client.player;
            if (player == null)
            {
                client.Disconnect("당신은 아직 로그인하지 않았습니다.");
                return;
            }

            // Check entry in the Room
            RoomInstance room = player.room;
            if (room == null || (!room.players.Contains(player) && !room.observers.Contains(player)))
            {
                client.Disconnect("당신이 속해있는 방을 찾지 못했습니다.");
                return;
            }

            lock (room)
            {
                // handling
                switch (code)
                {
                    case ClientOpcode.Room_Exit:
                        {
                            player.ExitRoom();
                        }
                        break;
                    case ClientOpcode.Room_Ban:
                        {
                            if (room.IsMaster(player))
                            {
                                client.Msg("방장권한이 없습니다.");
                                break;
                            }

                            int banTargetID = r.ReadD();
                            PlayerInstance banTarget = null;
                            for (int i = 0; i < room.players.Count; i++)
                            {
                                if (room.players[i].instanceID == banTargetID)
                                {
                                    room.RemovePlayer(banTarget = room.players[i]);
                                    break;
                                }
                            }

                            // Broadcast Ban User Info
                            byte[] data = S_Room.Ban(banTargetID);
                            room.Broadcast(data);
                            banTarget.Send(data);
                        }
                        break;
                    case ClientOpcode.Room_ChangeLevel:
                        {
                            short mapID = r.ReadH();
                            room.MapID = mapID;
                            room.BroadcastRoomInfo();
                        }
                        break;
                    case ClientOpcode.Room_Ready:
                        {
                            bool isReady = r.ReadB();

                            player.IsReady = isReady;
                            player.Broadcast(player.PacketInfo());
                            player.isLoaded = false;

                            // 모두 레디를 했는지 검사
                            room.CheckAlready();
                        }
                        break;
                    case ClientOpcode.Room_LoadComplete:
                        {
                            if (!room.IsLoadStarted)
                            {
                                room.ForceStart(false);
                            }

                            player.isLoaded = true;
                            room.AddLoadedPlayer(player);
                        }
                        break;
                    case ClientOpcode.Room_MapStage:
                        {
                            var stage = r.ReadH();
                            room.MapStage = room.MapStage > stage ? room.MapStage : stage;
                            //SLog.d("room.MapStage: " + room.MapStage);
                            break;
                        }
                    case ClientOpcode.Room_End:
                        {
                            try
                            {
                                RoomManager.GetNum(room.Num);
                                string param = $"sn={room.Num}";
                                HttpHelper.GetHTTP($"/api/Game/RoomEnd?{param}");
                                
                            }
                            catch (Exception e)
                            {
                                SLog.e("Error : ClientOpcode.Room_End ", e);
                            }
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// 씬에 들어간 뒤의 패킷 처리
        /// </summary>
        /// <param name="code"></param>
        /// <param name="r"></param>
        void OnScenePacket(ClientOpcode code, BinaryReader r)
        {
            PlayerInstance player = client.player;
            if (player == null)
            {
                client.Disconnect("당신은 아직 로그인하지 않았습니다.");
                return;
            }

            // Check entry in the Room
            RoomInstance room = player.room;
            if (room == null || (!room.players.Contains(player) && !room.observers.Contains(player)))
            {
                client.Disconnect("당신이 속해있는 방을 찾지 못했습니다.");
                return;
            }

            lock (room)
            {
                // handling
                switch (code)
                {
                    //case ClientOpcode.Scene_Initialized:
                    //    {
                    //        int instanceId = r.ReadD();
                    //        PlayerInstance target = room.FindByInstanceID(instanceId);
                    //        if (target != null)
                    //        {
                    //            target.Send(S_Scene.Start());
                    //        }
                    //    }
                    //    break;

                    case ClientOpcode.Scene_UpdateScore:
                        {
                            int instanceId = r.ReadD();
                            byte type = r.ReadC();
                            int value = r.ReadD();

                            PlayerInstance target = (instanceId == 0 ? player : room.FindByInstanceID(instanceId));
                            if (target != null)
                            {
                                switch (type)
                                {
                                    case 0: // Absolute
                                        {
                                            room.score.Remove(target.score);
                                            target.score = value;
                                            room.score.Add(target.score);
                                        }
                                        break;
                                    case 1: // Relative
                                        {
                                            room.score.Remove(target.score);
                                            target.score += value;
                                            room.score.Add(target.score);
                                        }
                                        break;
                                }
                                var rankinglist = from w in room.score orderby w descending select w;
                                for (short j = 0; j < rankinglist.Count(); ++j)
                                {
                                    if (rankinglist.ElementAt<int>(j) == target.score)
                                    {
                                        target.ranking = ++j;
                                        //SLog.d("instanceId: " + instanceId +"      "+ target.name + " 순위는 " + target.ranking +  "\n");
                                        break;
                                    }
                                }
                                //자신의 랭킹은 변경 되었지만 다른 유저의 랭킹은 변화 없음 -> 다른유저의 랭킹변화는 roominfo 패킷에서 진행.. 시간오래걸리니까
                                //SLog.d("Id: " + target.instanceID + "      " + " 점수는 " + target.score + "\n");
                                room.bRranking = true;
                                target.Send(target.PacketInfo(), true, false);
                                //target.Broadcast(target.PacketInfo(), null, true, false);
                            }
                        }
                        break;

                    case ClientOpcode.Scene_SetData:
                        lock (room.data)
                        {
                            S_Data.TYPE type = (S_Data.TYPE)r.ReadC();
                            string key = r.ReadS();

                            switch (type)
                            {
                                case S_Data.TYPE.String:
                                    {
                                        room.data.SetString(key, r.ReadS());
                                    }
                                    break;
                                case S_Data.TYPE.Int:
                                    {
                                        room.data.SetInt(key, r.ReadD());
                                    }
                                    break;
                                case S_Data.TYPE.Float:
                                    {
                                        room.data.SetFloat(key, r.ReadF());
                                    }
                                    break;
                                case S_Data.TYPE.Bool:
                                    {
                                        room.data.SetBool(key, r.ReadB());
                                    }
                                    break;
                                case S_Data.TYPE.Vector3:
                                    {
                                        room.data.SetVector3(key, r.ReadVector3());
                                    }
                                    break;
                                case S_Data.TYPE.Color:
                                    {
                                        room.data.SetColor(key, r.ReadColorRGBA());
                                    }
                                    break;
                            }
                        }
                        break;

                    case ClientOpcode.Scene_RequestCommanderID:
                        lock (room.commanderID)
                        {
                            var path = r.ReadS();
                            var type = r.ReadS();
                            var combine = path + '/' + type;

                            uint commanderId;
                            if (!room.commanderID.TryGetValue(combine, out commanderId))
                            {
                                room.commanderID.Add(combine, commanderId = room.nextCommanderID++);
                            }

                            player.Send(S_Scene.SetupCommanderID(path, type, commanderId));
                        }
                        break;
                }
            }
        }

        void OnSafetyPacket(ClientOpcode code, BinaryReader r)
        {
            PlayerInstance player = client.player;
            if (player == null)
            {
                client.Disconnect("당신은 아직 로그인하지 않았습니다.");
                return;
            }

            // Check entry in the Room
            RoomInstance room = player.room;
            if (room == null || (!room.players.Contains(player) && !room.observers.Contains(player)))
            {
                client.Disconnect("당신이 속해있는 방을 찾지 못했습니다.");
                return;
            }

            lock (room)
            {
                switch (code)
                {
                    case ClientOpcode.Safety_Contents_End:
                        {
                            if (client.player != null)
                            {
                                //현재 종료된 콘텐츠 번호
                                short stage = r.ReadH();
                                //최대 기다리는 시간
                                int ms = r.ReadD();
                                bool skip = r.ReadB();
                                if (skip)
                                {
                                    for(int i=0; i<room.players.Count; ++i)
                                    {
                                        room.players[i].contentsEnd = false;
                                        room.players[i].quiz = 0;
                                    }
                                    room.MapStage++;
                                    SLog.d("Room " + room.Num + " : MapStage " + room.MapStage + "변경");
                                    room.DetailMapStage = 0;
                                    break;
                                }
                                player.contentsEnd = true;
                                //현재 실행중인 방 정보와 종료되었다고 보낸 방 정보가 일치해야지만 실행..
                                //그외는 버그로 인식
                                if (stage == room.MapStage)
                                {
                                    //모든 유저가 종료시 바로 다음것으로 진행.
                                    //한명이 종료라면 ms초뒤 mapstage 증가
                                    //한명 이상이 종료라면 그냥 아무것도 안함.
                                    int count = 0;
                                    foreach (var user in room.players)
                                    {
                                        if (user.contentsEnd == true) { count++; }
                                    }

                                    if (count == room.players.Count)
                                    {
                                        for (int i = 0; i < room.players.Count; ++i)
                                        {
                                            room.players[i].contentsEnd = false;
                                            room.players[i].quiz = 0;
                                        }
                                        //room.MapStage = (short)(room.MapStage > stage + 1 ? room.MapStage : stage + 1);
                                        room.MapStage++;
                                        SLog.d("Room " + room.Num + " : MapStage " + room.MapStage + "변경");
                                        room.DetailMapStage = 0;
                                    }
                                    else if (count == 1)
                                    {
                                        //처음 Safety_Contents_End 패킷을 받을때 ms 초뒤 해당 이벤트 실행
                                        //단 이미 room.MapStage가 증가했다면 실행을 안한다.
                                        var task = Task.Run(() =>
                                        {
                                            Thread.Sleep(ms);
                                            if (stage == room.MapStage)
                                            {
                                                for (int i = 0; i < room.players.Count; ++i)
                                                {
                                                    room.players[i].contentsEnd = false;
                                                    room.players[i].quiz = 0;
                                                }
                                                //room.MapStage = (short)(room.MapStage > stage + 1 ? room.MapStage : stage + 1);
                                                room.MapStage++;
                                                SLog.d("Room " + room.Num + " : " + ms + "ms 대기후, MapStage " + room.MapStage + "변경");
                                                room.DetailMapStage = 0;
                                            }
                                        });
                                    }
                                }
                                else
                                {
                                    if(stage > room.MapStage)
                                    {
                                        room.MapStage = stage;
                                        client.Log($"Room : {room.Num} Error... 현재 MapStage {room.MapStage}, 종료되었다고 받은 Stage {stage}, Room.MapStage 강제 변경");
                                    }
                                    //client.Log($"Room : {room.Num} Error... 현재 MapStage {room.MapStage}, 종료되었다고 받은 Stage {stage}");
                                }
                            }
                            else
                            {
                                client.LogWarning("Not initialized player. but still client sent Safety_Contents_End");
                            }
                        }
                        break;
                    case ClientOpcode.Safety_Quiz_End:
                        {
                            if (client.player != null)
                            {
                                short stage = r.ReadH();
                                int ms = r.ReadD();
                                bool skip = r.ReadB();
                                if (skip)
                                {
                                    for (int i = 0; i < room.players.Count; ++i)
                                    {
                                        room.players[i].contentsEnd = false;
                                        room.players[i].quiz = 0;
                                    }
                                    room.DetailMapStage++;
                                    SLog.d("Room " + room.Num + " : DetailMapStage " + room.DetailMapStage + "변경");
                                    room.Broadcast(S_Safety.NextQuiz(room.DetailMapStage));
                                    break;
                                }
                                player.summary_QuizEnd = true;
                                if (stage == room.DetailMapStage)
                                {
                                    //모든 유저가 종료시 바로 다음것으로 진행.
                                    //한명이 종료라면 ms초뒤 mapstage 증가
                                    //한명 이상이 종료라면 그냥 아무것도 안함.
                                    int count = 0;
                                    foreach (var user in room.players)
                                    {
                                        if (user.summary_QuizEnd == true) { count++; }
                                    }

                                    if (count == room.players.Count)
                                    {
                                        for (int i = 0; i < room.players.Count; ++i)
                                        {
                                            room.players[i].contentsEnd = false;
                                            room.players[i].quiz = 0;
                                        }
                                        //room.DetailMapStage = (short)(room.DetailMapStage > stage + 1 ? room.DetailMapStage : stage + 1);
                                        room.DetailMapStage++;
                                        SLog.d("Room " + room.Num + " : DetailMapStage " + room.DetailMapStage + "변경");
                                        room.Broadcast(S_Safety.NextQuiz(room.DetailMapStage));
                                    }
                                    else if (count == 1)
                                    {
                                        var task = Task.Run(() =>
                                        {
                                            Thread.Sleep(ms);
                                            if (stage == room.DetailMapStage)
                                            {
                                                for (int i = 0; i < room.players.Count; ++i)
                                                {
                                                    room.players[i].contentsEnd = false;
                                                    room.players[i].quiz = 0;
                                                }
                                                //room.DetailMapStage = (short)(room.DetailMapStage > stage + 1 ? room.DetailMapStage : stage + 1);
                                                room.DetailMapStage++;
                                                SLog.d("Room " + room.Num + " : " + ms + "ms 대기후, DetailMapStage " + room.DetailMapStage + "변경");
                                                room.Broadcast(S_Safety.NextQuiz(room.DetailMapStage));
                                            }
                                        });
                                    }
                                }
                                else
                                {
                                    //client.Log($"Room: {room.Num} Error... 현재 MapStage의 세부Stage {room.DetailMapStage}, 종료되었다고 받은 Stage {stage}");
                                }
                            }
                            else
                            {
                                client.LogWarning("Not initialized player. but still client sent Safety_Quiz_End");
                            }
                        }
                        break;

                    case ClientOpcode.Safety_Request_DetailMapStage:
                        {
                            player.Send(S_Safety.DetailMapStage(room.DetailMapStage));
                        }
                        break;
                    case ClientOpcode.Safety_Start:
                        {
                            player.ready = true;
                            for (int i = 0; i < room.players.Count; ++i)
                            {
                                if (!room.players[i].ready)
                                {
                                    var task = Task.Run(() =>
                                    {
                                        if (room.bStart)
                                        {
                                            room.Broadcast(S_Safety.MapStageStart(room.MapStage));
                                            return;
                                        }
                                        Thread.Sleep(10000);
                                        if (!room.bStart)
                                        {
                                            room.bStart = true;
                                            room.MapID = 2;
                                            room.Broadcast(S_Safety.MapStageStart(room.MapStage));
                                            client.log.debug("Safety Start");
                                        }
                                    });
                                    return;
                                }
                            }
                            room.bStart = true;
                            room.MapID = 2;
                            //시작 패킷
                            room.Broadcast(S_Safety.MapStageStart(room.MapStage));
                            client.log.debug("Safety Start");
                        }
                        break;
                    default:
                        client.LogWarningFormat("Unkown Opcode '{0}({1})'", code.ToString(), (int)code);
                        break;
                }
            }
        }

        void OnBrainPacket(ClientOpcode code, BinaryReader r)
        {
            PlayerInstance player = client.player;
            if (player == null)
            {
                client.Disconnect("당신은 아직 로그인하지 않았습니다.");
                return;
            }

            // Check entry in the Room
            RoomInstance room = player.room;
            if (room == null || (!room.players.Contains(player) && !room.observers.Contains(player)))
            {
                client.Disconnect("당신이 속해있는 방을 찾지 못했습니다.");
                return;

            }

            lock (room)
            {
                switch (code)
                {
                    case ClientOpcode.Brain_Index:
                        {
                            short index = r.ReadH();
                            room.Broadcast(S_Brain.StartIndex(index));
                        }
                        break;
                    default:
                        client.LogWarningFormat("Unkown Opcode '{0}({1})'", code.ToString(), (int)code);
                        break;
                }
            }
        }

        void OnCNUHPacket(ClientOpcode code, BinaryReader r)
        {
            PlayerInstance player = client.player;
            if (player == null)
            {
                client.Disconnect("당신은 아직 로그인하지 않았습니다.");
                return;
            }

            // Check entry in the Room
            RoomInstance room = player.room;
            if (room == null || (!room.players.Contains(player) && !room.observers.Contains(player)))
            {
                client.Disconnect("당신이 속해있는 방을 찾지 못했습니다.");
                return;

            }

            lock (room)
            {
                switch (code)
                {
                    case ClientOpcode.CNUH_Ready:
                        {
                            player.ready = true;
                            for (int i = 0; i < room.players.Count; ++i)
                            {
                                if (!room.players[i].ready)
                                {
                                    var task = Task.Run(() =>
                                    {
                                        if (room.bStart)
                                        {
                                            room.Broadcast(S_CNUH.MapStageStart(room.MapStage));
                                            return;
                                        }
                                        Thread.Sleep(10000);
                                        if (!room.bStart)
                                        {
                                            room.bStart = true;
                                            room.MapID = 1;
                                            room.Broadcast(S_CNUH.MapStageStart(room.MapStage));
                                            client.log.debug("10s sleep.. Start");
                                        }
                                    });
                                    return;
                                }
                            }
                            room.bStart = true;
                            room.MapID = 1;
                            //시작 패킷
                            room.Broadcast(S_CNUH.MapStageStart(room.MapStage));
                            room.videoStartTime = DateTime.Now;
                            room.videoTimeMs = 0;
                            client.log.debug("all device ready.. Start");
                        }
                        break;
                    default:
                        client.LogWarningFormat("Unkown Opcode '{0}({1})'", code.ToString(), (int)code);
                        break;
                }
            }
        }

        void OnVideoPacket(ClientOpcode code, BinaryReader r)
        {
            PlayerInstance player = client.player;
            if (player == null)
            {
                client.Disconnect("당신은 아직 로그인하지 않았습니다.");
                return;
            }

            // Check entry in the Room
            RoomInstance room = player.room;
            if (room == null || (!room.players.Contains(player) && !room.observers.Contains(player)))
            {
                client.Disconnect("당신이 속해있는 방을 찾지 못했습니다.");
                return;

            }

            lock (room)
            {
                switch (code)
                {
                    case ClientOpcode.Video_Ready:
                        {
                            player.ready = true;
                            for (int i = 0; i < room.players.Count; ++i)
                            {
                                if (!room.players[i].ready)
                                {
                                    var task = Task.Run(() =>
                                    {
                                        if (room.bStart)
                                        {
                                            room.Broadcast(S_Video.MapStageStart(room.MapStage));
                                            return;
                                        }
                                        Thread.Sleep(10000);
                                        if (!room.bStart)
                                        {
                                            room.bStart = true;
                                            room.MapID = 1;
                                            room.Broadcast(S_Video.MapStageStart(room.MapStage));
                                            room.videoTimeMs = 0;
                                            client.log.debug("10s sleep.. Start");
                                        }
                                    });
                                    return;
                                }
                            }
                            room.bStart = true;
                            room.MapID = 1;
                            //시작 패킷
                            room.Broadcast(S_Video.MapStageStart(room.MapStage));
                            room.videoStartTime = DateTime.Now;
                            room.videoTimeMs = 0;
                            client.log.debug("all device ready.. Start");
                        }
                        break;
                    default:
                        client.LogWarningFormat("Unkown Opcode '{0}({1})'", code.ToString(), (int)code);
                        break;
                }
            }
        }
    }
}