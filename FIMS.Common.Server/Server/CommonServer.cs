﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeNet;

namespace FTF.Server
{
    class CommonServer
    {
        private static List<Client> userlist;

        private static CNetworkService service;

        public static void Start(int port)
        {
            if (userlist == null)
            {
                userlist = new List<Client>();
            }

            if (service == null)
            {
                service = new CNetworkService(true);
            }

            // 콜백 매소드 설정.
            service.session_created_callback += on_session_created;
            // 초기화.
            service.initialize(10000, 4096);
            service.listen("0.0.0.0", port, 100);
            //service.disable_heartbeat();
        }

        static void on_session_created(CUserToken token)
        {
            Client user = new Client(token);
            lock (userlist)
            {
                userlist.Add(user);
            }
        }


        public static void remove_user(Client user)
        {
            lock (userlist)
            {
                userlist.Remove(user);
            }
        }
    }
}
