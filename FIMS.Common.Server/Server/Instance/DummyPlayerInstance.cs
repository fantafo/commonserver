﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using FTF.Server.Packet;

namespace FTF.Server
{
    public class DummyPlayerInstance : PlayerInstance
    {
        public override bool IsDummy => true;
    }
}
