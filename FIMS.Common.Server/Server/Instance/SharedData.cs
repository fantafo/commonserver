﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using FTF.Server.Packet;
using System.Threading;

namespace FTF.Server
{
    /// <summary>
    /// (실험적)
    /// 
    /// 유저들의 데이터들을 통합하기 위해 사용한다.
    /// FIMS.SDK의 Data.cs 를 이용하여 정의한 데이터들을 저장해뒀다가,
    /// 늦게 들어온 유저 혹은 데이터가 누락된 유저들에게 보내주는 역할을 하고 있는 클래스다.
    /// </summary>
    public class SharedData
    {

        public class Entry<T>
        {
            public bool isDirty = true;
            public T value;

            public Entry(T val) { value = val; }
        }

        public RoomInstance room;

        public bool isDirty;
        public Dictionary<string, Entry<string>> stringData = new Dictionary<string, Entry<string>>();
        public Dictionary<string, Entry<int>> intData = new Dictionary<string, Entry<int>>();
        public Dictionary<string, Entry<float>> floatData = new Dictionary<string, Entry<float>>();
        public Dictionary<string, Entry<bool>> boolData = new Dictionary<string, Entry<bool>>();
        public Dictionary<string, Entry<Vector3>> vectorData = new Dictionary<string, Entry<Vector3>>();
        public Dictionary<string, Entry<Color32>> colorData = new Dictionary<string, Entry<Color32>>();


        ///////////////////////////////////////////////////////////////////
        /// 
        /// 데이터 세팅용 메서드
        /// 
        ///////////////////////////////////////////////////////////////////

        public void SetString(string key, string value)
        {
            lock (this)
            {
                Entry<string> result;
                if (stringData.TryGetValue(key, out result))
                {
                    if (result.value != value)
                    {
                        isDirty = true;
                        result.isDirty = true;
                        result.value = value;
                    }
                }
                else
                {
                    stringData.Add(key, new Entry<string>(value));
                }
            }
        }
        public void SetInt(string key, int value)
        {
            lock (this)
            {
                Entry<int> result;
                if (intData.TryGetValue(key, out result))
                {
                    if (result.value != value)
                    {
                        isDirty = true;
                        result.isDirty = true;
                        result.value = value;
                    }
                }
                else
                {
                    intData.Add(key, new Entry<int>(value));
                }
            }
        }
        public void SetFloat(string key, float value)
        {
            lock (this)
            {
                Entry<float> result;
                if (floatData.TryGetValue(key, out result))
                {
                    if (result.value != value)
                    {
                        isDirty = true;
                        result.isDirty = true;
                        result.value = value;
                    }
                }
                else
                {
                    floatData.Add(key, new Entry<float>(value));
                }
            }
        }
        public void SetBool(string key, bool value)
        {
            lock (this)
            {
                Entry<bool> result;
                if (boolData.TryGetValue(key, out result))
                {
                    if (result.value != value)
                    {
                        isDirty = true;
                        result.isDirty = true;
                        result.value = value;
                    }
                }
                else
                {
                    boolData.Add(key, new Entry<bool>(value));
                }
            }
        }
        public void SetVector3(string key, Vector3 value)
        {
            lock (this)
            {
                Entry<Vector3> result;
                if (vectorData.TryGetValue(key, out result))
                {
                    if (result.value != value)
                    {
                        isDirty = true;
                        result.isDirty = true;
                        result.value = value;
                    }
                }
                else
                {
                    vectorData.Add(key, new Entry<Vector3>(value));
                }
            }
        }
        public void SetColor(string key, Color32 value)
        {
            lock (this)
            {
                Entry<Color32> result;
                if (colorData.TryGetValue(key, out result))
                {
                    if (result.value != value)
                    {
                        isDirty = true;
                        result.isDirty = true;
                        result.value = value;
                    }
                }
                else
                {
                    colorData.Add(key, new Entry<Color32>(value));
                }
            }
        }


        ///////////////////////////////////////////////////////////////////
        /// 
        /// 데이터 전송용 메서드
        /// 
        ///////////////////////////////////////////////////////////////////

        public void Send(PlayerInstance player)
        {
            lock (this)
            {
                Dictionary<string, object> temp = new Dictionary<string, object>();

                SendTarget(player, stringData, temp);
                SendTarget(player, intData, temp);
                SendTarget(player, floatData, temp);
                SendTarget(player, boolData, temp);
                SendTarget(player, vectorData, temp);
                SendTarget(player, colorData, temp);

                if (temp.Count > 0)
                    player.Send(S_Data.ObjectPackage(temp));
            }
        }
        void SendTarget<T>(PlayerInstance player, Dictionary<string, Entry<T>> data, Dictionary<string, object> temp)
        {
            var e = data.GetEnumerator();
            while (e.MoveNext())
            {
                temp.Add(e.Current.Key, e.Current.Value);
                if (temp.Count == 255)
                {
                    player.Send(S_Data.ObjectPackage(temp));
                    stringData.Clear();
                }
            }
        }

        public void Broadcast()
        {
            lock (this)
            {
                if(isDirty)
                {
                    Dictionary<string, object> temp = new Dictionary<string, object>();

                    BroadcastDictionary(stringData, temp);
                    BroadcastDictionary(intData, temp);
                    BroadcastDictionary(floatData, temp);
                    BroadcastDictionary(boolData, temp);
                    BroadcastDictionary(vectorData, temp);
                    BroadcastDictionary(colorData, temp);

                    if(temp.Count > 0)
                        room.BroadcastInGame(S_Data.ObjectPackage(temp));

                    isDirty = false;
                }
            }
        }
        void BroadcastDictionary<T>(Dictionary<string, Entry<T>> data, Dictionary<string, object> temp)
        {
            var e = data.GetEnumerator();
            while (e.MoveNext())
            {
                if (e.Current.Value.isDirty)
                {
                    e.Current.Value.isDirty = false;
                    temp.Add(e.Current.Key, e.Current.Value.value);
                    if (temp.Count == 255)
                    {
                        room.BroadcastInGame(S_Data.ObjectPackage(temp));
                        stringData.Clear();
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////
        /// 
        /// 룸 데이터 브로드케스트용 쓰레드 러너
        /// 
        ///////////////////////////////////////////////////////////////////

        private Thread thread;
        private bool isRun;
        public void Start()
        {
            if (!isRun)
            {
                isRun = true;
                thread = new Thread(runBroadcast);
                thread.Name = "DataBoradcaster-" + room.Num;
                thread.Start();
            }
        }
        public void Stop()
        {
            isRun = false;
        }

        private void runBroadcast()
        {
            while (isRun)
            {
                if (isDirty)
                {
                    Broadcast();
                }
                else
                {
                    Thread.Sleep(100);
                }
            }
        }
    }
}