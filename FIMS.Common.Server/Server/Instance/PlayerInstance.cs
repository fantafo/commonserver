﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using FTF.Server.Packet;

namespace FTF.Server
{
    /// <summary>
    /// ZID 게임에서 사용하는 플레이어 정보.
    /// 게임에 포함된 고유정보들을 가지고있다.
    /// </summary>
    public class PlayerInstance
    {
        // 플레이어가 속해 있는 인스턴스 정보들
        public Account account;
        public RoomInstance room;

        public bool IsConnected { get { return account != null && account.IsConnected; } }

        // 오브젝트 고유 변수
        private static int generateInstanceID = 1;
        public readonly int instanceID = generateInstanceID++;

        // 게임용 변수
        public string name { get { return account != null ? account.Name : "Unkown"; } set { account.Name = value; } }
        public int level { get { return account != null ? account.Level : 0; } }
        public bool isDead { get { return currentHP <= 0; } }

        //count룸에서 사용하는 준비
        public bool IsReady;
        public bool isLoaded;

        public short currentHP;
        public short maxHP;
        public int score;
        public short ranking=0;
        public short quiz = 0;
        //safety에서 준비가 완료됨을 전달.
        public bool ready = false;
        //현재 진행 중인 영상,게임이 종료 시 true
        public bool contentsEnd = false;
        //현재 진행 중인 summary_Quiz 한 문제 종료 시 true
        public bool summary_QuizEnd = false;
        //현재 진행 중인 혹은 실행할 영상,게임 인덱스 
        public int contentsIndex = 0;

        public string VoIP_Address = string.Empty;
        public int VoIP_Port;
        public int Voice_Frequency;
        public byte Voice_Channel;
        public int Voice_FrameSample;

        // 트래킹 정보
        public Vector3 headPosition = Vector3.zero;
        public Vector3 headRotation = Vector3.zero;
        public bool useHand = false;
        public Vector3 handPosition = Vector3.down;
        public Vector3 handRotation = Vector3.zero;
        public bool useReticle = false;
        public Vector3 reticlePosition = Vector3.zero;

        public float reticleScale = 0;
        public bool isObserver;
        public short bulletCount;

        public short headType { get { return account.AvatarHead; } }
        public short bodyType { get { return account.AvatarBody; } }
        public Client.ClientState clientState { get { return IsConnected ? account.clientState : Client.ClientState.Disconnect; } set { if (IsConnected) account.clientState = value; } }
        public virtual bool IsDummy { get { return account.client.IsDummy; } }

        #region Network
        /// <summary>
        /// 나에게만 메세지를 보낸다.
        /// </summary>
        public void Send(byte[] data, bool flush = true, bool throwing = false)
        {
            if (account.IsConnected)
            {
                account.Send(data, flush, throwing);
            }
            //else
            //{
            //    throw new NullReferenceException("Not found account");
            //}
        }

        /// <summary>
        /// exclude를 제외한 방에 있는 모두에게 메세지를 보낸다.
        /// </summary>
        public void Broadcast(byte[] data, bool flush = true, bool error = true)
        {
            if (room != null)
            {
                var rm = room;
                for (int i = 0; i < rm.players.Count; i++)
                {
                    if (rm.players[i] != this && rm.players[i].clientState > Client.ClientState.Channel)
                    {
                        rm.players[i].Send(data, flush);
                    }
                }
                for (int i = 0; i < rm.observers.Count; i++)
                {
                    rm.observers[i].Send(data, flush);
                }
            }
            else if(error)
            {
                throw new NullReferenceException("Not registed RoomInstance in PlayerInstance");
            }
        }

        /// <summary>
        /// exclude를 제외한 방에 있는 모두에게 메세지를 보낸다.
        /// </summary>
        public void Broadcast(byte[] data, PlayerInstance exclude, bool flush = true, bool error = true)
        {
            if (room != null)
            {
                var rm = room;
                for (int i = 0; i < rm.players.Count; i++)
                {
                    if (rm.players[i] != exclude && rm.players[i].clientState > Client.ClientState.Channel)
                    {
                        rm.players[i].Send(data, flush);
                    }
                }
                for (int i = 0; i < rm.observers.Count; i++)
                {
                    rm.observers[i].Send(data, flush);
                }
            }
            else if(error)
            {
                throw new NullReferenceException("Not registed RoomInstance in PlayerInstance");
            }
        }
        /// <summary>
        /// 클라이언트가 종료됐을때의 프로세스. Account에서 호출해준다.
        /// </summary>
        public void OnDisconnect()
        {
            isLoaded = false;
            IsReady = false;
            VoIP_Address = "";
            VoIP_Port = 0;
            ExitRoom();
        }

        #region Packet
        public byte[] PacketInfo()
        {
            return S_Common.CharacterInfo(this);
        }
        #endregion Packet
        #endregion Network


        public void ExitRoom()
        {
            var tempRoom = room; // sync로 인한 임시 할당
            if (tempRoom != null)
            {
                if (tempRoom != null)
                {
                    if (tempRoom.IsStarted || tempRoom.IsFixedRoom)
                    {
                        tempRoom.Broadcast(PacketInfo(), this);
                        tempRoom.CheckAllExit();
                    }
                    // 시스템이 생성한 방이 아닐경우에는 플레이어를 방목록에서 제외한다.
                    else
                    {
                        IsReady = false;
                        score = 0;

                        tempRoom.RemovePlayer(this);
                        tempRoom.BroadcastRoomInfo();
                    }
                }
            }
        }

        public void OnHit(int damage)
        {
        }

        public void Log(string v)
        {
            if(account.client != null)
            {
                account.client.Log(v);
            }
        }

        public override string ToString()
        {
            string txt = "";
            if(room != null)
            {
                int idx = room.players.IndexOf(this);
                if(idx == 0)
                {
                    txt += "[Master] ";
                }
                else
                {
                    txt += $"[{idx}] ";
                }
            }
            txt += name;
            return txt;
        }
    }
}
