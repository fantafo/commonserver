﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using FTF.Server.Packet;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace FTF.Server
{
    /// <summary>
    /// 유저들이 대기하고 게임을 시작할 수 있는 방 정보로서,
    /// 방번호, 제목, 등의 기본 방 설정을 담고있다.
    /// 
    /// RoomInstance를 통해서 GameInstance가 생성되며 GameInstance에 접근하기 위해서는
    /// 해당 클래스를 통과해야한다.
    /// 
    /// 0번째 플레이어는 방장으로 취급한다.
    /// </summary>
    public class RoomInstance
    {
        // 연결된 게임정보
        public bool IsFixedRoom { get; set; } // (FIMS에 의해생성) 사용자가 중간에 나가더라도 방목록에서 제외되지 않고, 모든 사용자가 종료하더라도 방이 사라지지 않습니다.
        public bool IsStarted { get; set; }
        public bool IsLoadStarted { get; set; }
        public DateTime startTime;
        public int elapsedTime => (IsStarted ? (int)(DateTime.Now - startTime).TotalSeconds : 0);

        public bool isDestroy; // true를 설정할 시, 다음 Frame Update에서 해당 룸의 Game정보를 삭제한다.
        public SLog log;

        // 방 기본정보
        public long Num;
        public string Password;
        public string Name;
        public byte MaxUser = 50;
        public short MapID = 1;
        public bool Fast = false;   //빠른시작시 true
        //safety에서만 사용, 시작시 true로 변 경
        public bool bStart = false;
        //safety에서는 safetymanager에서 동영상 재생번호
        public short MapStage = 0;
        public short DetailMapStage = 0;
        public short Level = 1; //콘텐츠 레벨
        public string Detail = ""; //360영상을 위해 제작. 실행할 영상 파일명이 들어감.
        public short Stage;
        public List<int> score;
        public bool bRranking = false;

        public string ClientIdentifier;
        public int ClientVersion;

        public bool b_Observer = false;

        //비디오 동기화를 위해
        public DateTime videoStartTime;
        public float videoTimeMs = -1;

        // 방 데이터
        public SharedData data;
        public uint nextCommanderID = 1;
        public Dictionary<string, uint> commanderID = new Dictionary<string, uint>(); 

        // 방 이벤트
        public event Action<RoomInstance, PlayerInstance> OnNewPlayer;
        public event Action<RoomInstance, PlayerInstance> OnRemovePlayer;
        public event Action<RoomInstance, PlayerInstance> OnMasterPlayer;

        // 유저 정보
        public List<PlayerInstance> players = new List<PlayerInstance>(); // 들어와 있는 유저
        public List<PlayerInstance> observers = new List<PlayerInstance>(); // 들어와 있는 유저
        public List<PlayerInstance> loadedPlayers = new List<PlayerInstance>(); // 로딩시작 후 로딩이 완료된 유저


        public void OnCreated()
        {
            log = new SLog("Room:" + Num);
            log.debug("created");
            score = new List<int>();
        }

        #region 유저정보 관련 메서드

        /// <summary>
        /// 해당 플레이어를 방 플레이어 목록에 추가한다.
        /// </summary>
        public bool AddPlayer(PlayerInstance player)
        {
            if (player.isObserver)
            {
                if (!observers.Contains(player))
                {
                    player.room = this;
                    observers.Add(player);
                    log.debug("add observer " + player.instanceID + "/" + player.name);
                    return true;
                }
            }
            else if (players.Count < MaxUser)
            {
                if (!players.Contains(player))
                {
                    player.room = this;
                    players.Add(player);
                    log.debug("add player " + player.instanceID + "/" + player.name);
                    OnNewPlayer?.Invoke(this, player);
                    return true;
                }
            }else if (players.Contains(player))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// 해당 플레이어를 방 플레이어 목록에서 제거한다.
        /// 목록에서 제거하며 LoadedPlayers리스트에서도 같이 제거한다.
        /// 만약 플레이어가 아무도 남지 않았다면 게임을 종료한다.
        /// </summary>
        /// <param name="player"></param>
        public void RemovePlayer(PlayerInstance player)
        {
            if (observers.Contains(player))
            {
                log.debug("remove observer " + player.instanceID + "/" + player.name);
                observers.Remove(player);
                player.room = null;
            }
            if (players.Contains(player))
            {
                log.debug("remove player " + player.instanceID + "/" + player.name);
                players.Remove(player);
                loadedPlayers.Remove(player);
                OnRemovePlayer?.Invoke(this, player);
                player.room = null;
            }

            if (players.Count == 0)
            {
                ForceStop();
            }
        }

        public void ChangeMaster(PlayerInstance player)
        {
            int idx = players.IndexOf(player);
            if (idx != -1)
            {
                players.RemoveAt(idx);
                players.Insert(0, player);
                BroadcastPlayersInfos();
                OnMasterPlayer?.Invoke(this, player);
            }
            else
            {
                log.debug(player + "는 방에 포함돼 있지 않습니다.");
            }
        }

        public PlayerInstance FindByInstanceID(int instanceID)
        {
            for (int i = 0; i < players.Count; i++)
            {
                if (players[i].instanceID == instanceID)
                {
                    return players[i];
                }
            }
            return null;
        }
        /// <summary>
        /// 해당 플레이어가 운영자이거나 방장인지 검사한다.
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public bool IsMaster(PlayerInstance player)
        {
            return player.account.IsGameMaster
                || (players.Count > 0 && player == this.players[0]);
        }

        public bool Contains(PlayerInstance player)
        {
            return players.Contains(player);
        }
        #endregion 유저정보 관련 메서드
        #region Network
        /// <summary>
        /// 유저가 룸에 잇을때의 정보를 전송한다..
        /// </summary>
        public void Broadcast(byte[] packet, bool flush = true)
        {
            if (isDestroy)
                return;
            for (int i = 0; i < this.players.Count; i++)
            {
                if (this.players[i].clientState >= Client.ClientState.Room)
                {
                    this.players[i].Send(packet, flush);
                    //this.players[i]
                }
            }
            for (int i = 0; i < this.observers.Count; i++)
            {
                this.observers[i].Send(packet, flush);
            }
        }

        /// <summary>
        /// 유저 전부에게 데이터를 전송한다.
        /// </summary>
        public void BroadcastInGame(byte[] packet, bool flush = true)
        {
            if (isDestroy)
                return;
            for (int i = 0; i < this.players.Count; i++)
            {
                if (this.players[i].clientState >= Client.ClientState.Room && (!IsStarted || this.players[i].isLoaded))
                    this.players[i].Send(packet, flush);
            }
            for (int i = 0; i < this.observers.Count; i++)
            {
                if (!IsStarted || this.observers[i].isLoaded)
                    this.observers[i].Send(packet, flush);
            }
        }
        /// <summary>
        /// exclude 유저를 제외한 플레이어들에게 정보를 전송한다.
        /// </summary>
        public void Broadcast(byte[] packet, PlayerInstance exclude, bool flush = true)
        {
            if (isDestroy)
                return;
            for (int i = 0; i < this.players.Count; i++)
            {
                if (this.players[i].clientState >= Client.ClientState.Room && (this.players[i] != exclude && (!IsStarted || this.players[i].isLoaded)))
                {
                    this.players[i].Send(packet, flush);
                }
            }
            for (int i = 0; i < this.observers.Count; i++)
            {
                if (!IsStarted || this.observers[i].isLoaded)
                    this.observers[i].Send(packet, flush);
            }
        }
        public void BroadcastRoomInfo()
        {
            if (isDestroy)
                return;
            Broadcast(PacketInfo());

            //비디오가 플레이중이라면 보낸다.
            if(videoTimeMs >= 0)
            {
                TimeSpan interval = DateTime.Now - videoStartTime;
                //log.debug("interval.TotalMilliseconds: " + interval.TotalMilliseconds);
                Broadcast(VideoInfo(interval.TotalMilliseconds));
            }
        }
        public void BroadcastPlayersInfos()
        {
            for (int i = 0; i < players.Count; i++)
            {
                Broadcast(players[i].PacketInfo());
            }
        }
        public void SendPlayersInfos(PlayerInstance player)
        {
            if ((IsStarted || player.isLoaded) && isDestroy)
                return;
            for (int i = 0; i < players.Count; i++)
            {
                player.Send(players[i].PacketInfo());
            }
        }
        public byte[] PacketInfo()
        {
            return S_Room.Info(this);
        }
        public byte[] VideoPlayReady()
        {
            return S_Video.PlayReady(Detail);
            //return S_Video.PlayReady("No._143_Solar_System_Tour.mp4");
        }
        public byte[] VideoInfo(double ms)
        {
            return S_Video.Info(ms);
        }
        #endregion Network
        #region GameStarter
        /// <summary>
        /// 모든 유저가 나갔는지 검사하고
        /// 모든 유저가 나갔다면 방을 종료한다.
        /// </summary>
        public void CheckAllExit()
        {
            for (int i = 0; i < this.players.Count; i++)
            {
                if (this.players[i].IsConnected)
                {
                    return;
                }
            }
            if (!IsFixedRoom)
                ForceStop();
        }
        /// <summary>
        /// 모든 유저가 레디상태인지를 검사한다.
        /// 모든 유저가 레디상태일경우 게임이 자동으로 시작된다.
        /// </summary>
        public void CheckAlready()
        {
            int readyCount = 0;
            for (int i = 0; i < players.Count; i++)
            {
                if (players[i].IsReady)
                    readyCount++;
            }

            if (!IsFixedRoom && readyCount == MaxUser)
            {
                SLog.d("GameStart " + Num);
                ForceStart(true, Level);
            }
        }
        /// <summary>
        /// 게임준비상태에서 모든 유저들이 게임플레이를 하기위한 로딩이 완료된경우
        /// 로딩된 유저로서 등록되며 로딩이 모두 완료되면 새로운 게임이 시작된다.
        /// </summary>
        public void AddLoadedPlayer(PlayerInstance player)
        {
            lock (this)
            {
                if (!player.isObserver)
                    loadedPlayers.Add(player);

                // 만약 최초의 로딩완료자 이후에 접속자가 있을경우 이후부터는
                // 접속자를 기준으로 초기화를 진행합니다.
                if (IsStarted)
                {
                    log.debug("Add Scene Player: "+player);
                }
                // 최초에 게임을 시작한 사람이 있을때 진짜 게임이 시작됩니다.
                else
                {
                    IsStarted = true;
                    startTime = DateTime.Now;
                    log.debug("Start Scene Player at "+player);
                }

                player.Send(PacketInfo());
                data.Send(player);
                //if(Detail.IsEmpty() == false)
                //{
                //    player.Send(VideoPlayInfo());
                //}

                PlayerInstance initer = null;
                // 플레이어 수만큼 반복하지만 성공한 경우(initer가 null이 아닌경우)에는 더이상 반복하지 않는다.
                for(int j=0; j<players.Count && initer == null; j++)
                {
                    // 플레이어중 접속중이거나 옵저버가 아니며 자신이 아닌 유저가 있다면,
                    // 해당 유저로부터 초기화를 받도록 요청한다.
                    for (int i = 0; i < players.Count; i++)
                    {
                        if (players[i].IsConnected && !players[i].IsDummy && players[i] != player)

                        {
                            initer = players[i];
                            break;
                        }
                    }
                    if (initer == null)
                    {
                        for (int i = 0; i < observers.Count; i++)
                        {
                            if (observers[i].IsConnected && observers[i] != player)
                            {
                                initer = observers[i];
                                break;
                            }
                        }
                    }

                    try
                    {
                        log.debug("initialized to " + initer.name + " -> " + player.name);
                        initer.Send(S_Scene.InitializeTo(player.instanceID));
                    }
                    catch
                    {
                        initer = null;
                    }
                }
            }
        }
        #endregion GameStarter

        public void ForceStop(bool calledServer = false)
        {
            lock (this)
            {
                if (isDestroy)
                    return;
                
                // 종료 패킷 전송
                Broadcast(S_Scene.End());

                //if (IsStarted || IsFixedRoom)
                //{
                //    log.debug("force stop S_Scene End");
                //    Broadcast(S_Scene.End());
                //}
                //else
                //{
                //    log.debug("force stop S_Room End");
                //    Broadcast(S_Room.Exit());
                //}

                //방정보 초기화
                MapStage = 0;
                bStart = false;

                // 플레이어 정보 초기화
                for (int i = 0; i < players.Count; i++)
                {
                    players[i].isLoaded = false;
                    players[i].IsReady = false;
                    players[i].room = null;
                    players[i].ranking = 0;
                    players[i].score = 0;
                    players[i].quiz = 0;
                    players[i].ready = false;
                }

                // 리스트 삭제
                if (data != null)
                {
                    data.Stop();
                }
                bStart = false;
                //비디오 시간 초기화
                videoTimeMs = -1;
                players.Clear();
                loadedPlayers.Clear();
                RoomManager.Remove(this);
                // 오브젝트 삭제 요청
                isDestroy = true;
            }
        }

        public void ForceStart(bool callLoadStart = true, short level =1)
        {
            if (IsStarted)
                return;

            foreach(var player in players)
            {
                if (player.clientState < Client.ClientState.Room)
                {
                    new Task(() => GameStart()).Start();
                    return;
                }
            }

            log.debug("Force Start");
            data = new SharedData { room = this };
            data.Start();
            loadedPlayers.Clear();
            IsLoadStarted = true;

            if(callLoadStart)
                Broadcast(S_Room.LoadStart(level));

            foreach(var player in players)
            {
                if (player is DummyPlayerInstance)
                    AddLoadedPlayer(player);
            }

            if(b_Observer)
                new Task(() => ObserverStart()).Start();
        }



        public bool JoinPlayer(PlayerInstance player)
        {
            if (!(player is DummyPlayerInstance))
            {
                ClientVersion = player.account.client.version;
                ClientIdentifier = player.account.client.identifier;
            }

            log.debug($"  -Join Player {player.instanceID} / {player.name}");
            player.Send(VideoPlayReady());
            bool isJoinedRoom = true;
            bool isStartLoading = true;

            // 이미 방이 시작돼 있는데 참여목록에 해당하는 방이라면?
            if (IsStarted && Contains(player))
            {
                player.Log("  -before access Room");
                player.clientState = Client.ClientState.Loading;
            }
            else if (AddPlayer(player) || Contains(player))
            {
                player.Log($"  -Enter Room: {Num}");
                
                // 게임이 이미 시작됐다면
                if (IsStarted)
                {
                    player.Log("  -Already Started");
                    player.clientState = Client.ClientState.Loading;
                }
                // 로드가 진행중이기에 다시 진행한다.
                else if (IsLoadStarted)
                {
                    player.isLoaded = false;
                    loadedPlayers.Remove(player);
                    isStartLoading = true;
                }
                // 아직 게임이 시작되지 않았다면
                else
                {
                    player.Log("  -Normal Enter");
                    player.IsReady = false;
                    player.score = 0;
                    score.Add(player.score);
                    player.clientState = Client.ClientState.Room;
                    isStartLoading = false;
                }
            }
            else
            {
                isJoinedRoom = false;
            }

            // 패킷 전송
            if (isJoinedRoom)
            {
                Broadcast(player.PacketInfo(), player); // 다른 유저에게 내 정보를 알려준다.
                player.Send(player.PacketInfo());   //플레이어에게 자신의 정보를 먼저 전달한다.
                SendPlayersInfos(player); // 참가자에게 다른 유저 정보를 알려준다.
                BroadcastRoomInfo(); // 방정보를 모두에게 갱신해준다.

                if (isStartLoading)
                {
                    if (player is DummyPlayerInstance)
                        AddLoadedPlayer(player);
                    else
                        player.Send(S_Room.LoadStart(player.room.Level)); // 게임 준비를 하라고 알려준다.
                }
            }
            return isJoinedRoom;
        }

        public override string ToString()
        {
            return $"{Num} (Name:{Name},Type:{ClientIdentifier}, Player:{players.Count})";
        }

        //옵저버 실행
        private void ObserverStart()
        {
            ProcessStartInfo psi = new ProcessStartInfo();
            string gameName = ClientIdentifier.Replace("com.fantafo.", "");
            string path = ServerSetting.ObserverPaths.GetString(gameName, "");
            if (path == "")
            {
                log.debug(path + "observer path is null");
                return;
            }
            psi.FileName = path;
            psi.WorkingDirectory = System.IO.Path.GetDirectoryName(psi.FileName);
            psi.CreateNoWindow = false;
            System.Diagnostics.Process.Start(psi);
        }

        //모든 플레이어가 준비가 되지 않았지만, 웹서버에서 start 하라고 보낼 경우 아래코드로 해결
        private void GameStart(bool callLoadStart = true, short level = 1)
        {
            while (true)
            {
                Thread.Sleep(1000);
                foreach (var player in players)
                {
                    if (player.clientState < Client.ClientState.Room)
                    {
                        continue;
                    }
                }
                ForceStart(callLoadStart, level);
                return;
            }
        }
        
    }
}