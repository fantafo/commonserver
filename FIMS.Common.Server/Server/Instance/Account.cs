﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTF.Server
{
    /// <summary>
    /// 아이디와 비밀번호 및 기본정보를 가지고 있는 계정.
    /// 해당 계정은 판타포 게임 시스템 전체에서 사용하는 계정정보다.
    /// </summary>
    public class Account
    {
        public Client client;
        public bool IsConnected { get { return client != null; } }
        public PlayerInstance player;

        // Default Data
        public long SN;
        public string Id;
        public string Password;
        public string Name;
        public string Uuid;

        public short AvatarHead = 1;
        public short AvatarBody = 1;

        // User Data
        public short Level = 1;
        public int Exp = 0;
        public bool IsGameMaster = false;

        public Client.ClientState clientState { get { return IsConnected ? client.state : Client.ClientState.Disconnect; } set { if (IsConnected) client.state = value; } }

        public void OnDisconnect()
        {
            client = null;
            if (player != null)
            {
                player.OnDisconnect();
            }
        }

        public void Send(byte[] data, bool flush = true, bool throwing = false)
        {
            if (client != null)
            {
                client.Send(data, flush, throwing);
            }
        }
    }
}