﻿using FTF;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

namespace FTF
{
    /// <summary>
    /// 서버를 개설한다.
    /// Socket을 이용하여 Listen을 실시하여 클라이언트의 접속을 감지하여 ClientInstance를 만든다.
    /// </summary>
    public static class ServerSetting
    {
        public static Properties setting = new Properties();
        public static void Load()
        {
            setting.Load("./FimsCommonServer_config/setting.prop");

            Version = setting.GetInt("version", Version);
            ServerPort = setting.GetInt("server.port", ServerPort);
            HttpPort = setting.GetInt("http.port", HttpPort);
            Publish = setting.GetBool("server.publish",Publish);

            ShowSendPacket = setting.GetBool("packet.send", ShowSendPacket);
            ShowReceivePacket = setting.GetBool("packet.receive", ShowReceivePacket);
            SavePackets = setting.GetBool("packet.save", SavePackets);
            SavePacketStack = setting.GetBool("packet.stack.save", SavePacketStack);

            FimsServerIP = setting.GetString("fims.ip", FimsServerIP);
            FimsServerPort = setting.GetInt("fims.port", FimsServerPort);

            ClientPingTiming = setting.GetInt("client.pingTiming", ClientPingTiming);
            ClientPingSkipError = setting.GetInt("client.pingSkipError", ClientPingSkipError);
            ClientReceiveTimeout = setting.GetInt("client.receiveTimeout", ClientReceiveTimeout);
            ClientSendTimeout = setting.GetInt("client.sendTimeout", ClientSendTimeout);

            AppReceiveTimeout = setting.GetInt("app.receiveTimeout", AppReceiveTimeout);
            AppSendTimeout = setting.GetInt("app.sendTimeout", AppSendTimeout);

            HttpTimeout = setting.GetInt("http.timeout", HttpTimeout);
            HttpTimeoutScale = setting.GetFloat("http.timeoutScale", HttpTimeoutScale);

            TransformDataSendIntervalTime = setting.GetInt("transformData.send.interval.time", TransformDataSendIntervalTime);
            VideoSynchronizationTime = setting.GetInt("VideoSynchronizationTime", VideoSynchronizationTime);

            string observerSettingPath = "./FimsCommonServer_config/observerSetting.prop";
            if (!File.Exists(observerSettingPath))
            {
                SLog.e("observerSetting Load", $"File Not Found '{Path.GetFullPath(observerSettingPath)}'");
            }
            else
            {
                string[] observerSetting = System.IO.File.ReadAllLines(observerSettingPath);
                if (observerSetting.Length > 0)
                {
                    foreach (string str in observerSetting)
                    {
                        string[] strSplit = str.Split('=');
                        if (strSplit[1] != null && strSplit[1] != "")
                            ObserverPaths.Add(strSplit[0], strSplit[1]);
                    }
                }
            }
            Console.WriteLine($"Version                     : {Version}");
            Console.WriteLine($"ServerPort                  : {ServerPort}");
            Console.WriteLine($"HttpPort                    : {HttpPort}");
            Console.WriteLine($"ServerPublish               : {Publish}");
            Console.WriteLine($"ShowSendPacket              : {ShowSendPacket}");
            Console.WriteLine($"ShowReceivePacket           : {ShowReceivePacket}");
            Console.WriteLine($"SavePackets                 : {SavePackets}");
            Console.WriteLine($"SavePacketStack             : {SavePacketStack}");
            Console.WriteLine($"FimsServerIP                : {FimsServerIP}");
            Console.WriteLine($"FimsServerPort              : {FimsServerPort}");
            Console.WriteLine($"ClientPingTiming            : {ClientPingTiming}ms");
            Console.WriteLine($"ClientPingSkipError         : {ClientPingSkipError} times");
            Console.WriteLine($"ClientReceiveTimeout        : {ClientReceiveTimeout}ms");
            Console.WriteLine($"ClientSendTimeout           : {ClientSendTimeout}ms");
            Console.WriteLine($"AppReceiveTimeout           : {AppReceiveTimeout}ms");
            Console.WriteLine($"AppSendTimeout              : {AppSendTimeout}ms");
            Console.WriteLine($"HttpTimeout                 : {HttpTimeout}ms");
            Console.WriteLine($"HttpTimeoutScale            : x{HttpTimeoutScale}");
            Console.WriteLine($"Transform.SendIntervalTime  : {TransformDataSendIntervalTime}ms");
            Console.WriteLine($"VideoSynchronizationTime  : {VideoSynchronizationTime}ms");
            Console.WriteLine($"===============ObserverPath===============");
            foreach(KeyValuePair<string, string> path in ObserverPaths)
            {
                Console.WriteLine($"{path.Key} : {path.Value}");
            }
            Console.WriteLine($"==========================================");
        }

        public static int Version = 3;
        public static int ServerPort = 41000;
        public static int HttpPort = 42000;
        public static bool Publish = false;

        public static bool ShowSendPacket = false;
        public static bool ShowReceivePacket = false;
        public static bool SavePackets = false;
        public static bool SavePacketStack = false;

        public static string FimsServerIP = "127.0.0.1";
        public static int FimsServerPort = 80;

        public static int ClientPingTiming = 1000;
        public static int ClientPingSkipError = 5;
        public static int ClientReceiveTimeout = 100000;
        public static int ClientSendTimeout = 10000;

        public static int AppReceiveTimeout = 7000;
        public static int AppSendTimeout = 7000;
        public static int HttpTimeout = 1000;
        public static float HttpTimeoutScale = 1;

        public static int TransformDataSendIntervalTime = 1000;
        //해당 시간보다 넘어갈경우 동기화
        public static int VideoSynchronizationTime = 1000;


        public static Dictionary<string, string> ObserverPaths = new Dictionary<string, string>();
    }
}