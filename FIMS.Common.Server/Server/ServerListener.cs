﻿using FTF;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;
using FreeNet;

namespace FTF.Server
{
    /// <summary>
    /// 서버를 개설한다.
    /// Socket을 이용하여 Listen을 실시하여 클라이언트의 접속을 감지하여 ClientInstance를 만든다.
    /// </summary>
    public class ServerListener
    {
        /********************************************************************************/
        /*                                                                              */
        /*                               Base Structure                                 */
        /*                                                                              */
        /********************************************************************************/
        static SLog log = new SLog("GameServer");

        static ServerListener _main;
        public static ServerListener main
        {
            get
            {
                if (_main == null)
                {
                    _main = new ServerListener();
                }
                return _main;
            }
        }


        /********************************************************************************/
        /*                                                                              */
        /*                               Static Variables                               */
        /*                                                                              */
        /********************************************************************************/
        public static bool IsRun { get; private set; }
        

        /********************************************************************************/
        /*                                                                              */
        /*                               Member Variables                               */
        /*                                                                              */
        /********************************************************************************/

        private Socket _listener;
        private Thread _listeningThread;


        /********************************************************************************/
        /*                                                                              */
        /*                               Operation Functions                            */
        /*                                                                              */
        /********************************************************************************/
        public bool StartServer()
        {
            if (_listeningThread == null)
            {
                IsRun = true;
                _listeningThread = new Thread(_listening);
                _listeningThread.Name = "Listening Thread";
                _listeningThread.Start();

                log.debug("Start Server");
                return true;
            }
            return false;
        }

        public void StopServer()
        {
            IsRun = false;

            if (_listener != null)
            {
                try
                { _listener.Dispose(); }
                catch (Exception e) { log.ex(e); }
                _listener = null;
            }

            _listeningThread.Join();
            _listeningThread = null;

            log.debug("Disconnect Clients");
            var clients = ClientManager.GetEnumerator();
            while (clients.MoveNext())
            {
                clients.Current.Disconnect();
            }

            log.debug("Stop Complete");
        }


        /********************************************************************************/
        /*                                                                              */
        /*                               Listening Runnable                             */
        /*                                                                              */
        /********************************************************************************/

        void _listening()
        {
            System.Random rnd = new System.Random();
            byte[] seedPacket = new byte[32];

            //-- 서버를 바인딩한다.
            //-- 만약 겹치는 포트가 존재한다면 다른 포트에 바인딩을 시도(최대 1000회)한다.
            //-- 접속이 완료되면 MySQL과 UserSetting의 ServerPort를 바꾼다.

            IPEndPoint ipEndPoint = null;
            while (true) 
            {
                try
                {
                    int availablePort = IpHelper.AvailablePortRange(ServerSetting.ServerPort, ServerSetting.ServerPort + 1000);
                    ipEndPoint = new IPEndPoint(IPAddress.Any, availablePort);
                    _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                    _listener.Bind(ipEndPoint);
                    _listener.Listen(100);

                    File.WriteAllText("port", ipEndPoint.Port.ToString());
                    ServerSetting.ServerPort = ipEndPoint.Port;
                    break;
                }
                catch (Exception e)
                {
                    log.ex(e);
                    Thread.Sleep(1000);
                }
            }
            log.debug("Binded " + ipEndPoint);

            //-- 실제 클라이언트를 대기하는 부분
            while (IsRun)
            {
                try
                {
                    log.debug("wait for client connected...");
                    Socket socket = _listener.Accept();
                    log.debug("client accepted " + ((IPEndPoint)socket.RemoteEndPoint).Address);

                    // 클라이언트를 만든다.
                    Client client = new Client(socket);

                    // 암호화를 위한 랜덤시드를 생성한 뒤, 클라이언트에 설정한다.
                    rnd.NextBytes(seedPacket);
                    client.SetSeed(seedPacket);
                }
                catch (SocketException e)
                {
                    if (IsRun)
                    {
                        
                        switch (e.SocketErrorCode)
                        {
                            case SocketError.Interrupted: // WSACancelBlockingCall를 호출하여 차단 작업이 중단되었습니다.
                                break; // 디버깅 과정 중 다량 발생하는 IO-Timeout과 같은 예외

                            default:
                                log.ex(e);
                                break;
                        }
                    }
                }
                catch (Exception e)
                {
                    if (IsRun)
                    {
                        log.ex(e);
                    }
                }
            }
        }

    }
}