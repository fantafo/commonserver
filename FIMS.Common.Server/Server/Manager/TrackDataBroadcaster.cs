﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;
using System.Threading;
using System.Linq;
using FTF.Server.Packet;


namespace FTF.Server
{
    /// <summary>
    /// 유저들의 트래킹데이터를 
    /// TransformDataSendIntervalTime(ms)마다 한번씩
    /// 지속적으로 전달하여 유저들의 움직임을 묘사할 수 있도록 한다.
    /// </summary>
    public static class TrackDataBroadcaster
    {
        static bool isRun;
        static Thread thread;

        public static void Start()
        {
            if (!isRun)
            {
                isRun = true;
                thread = new Thread(run);
                thread.Name = "TrackDataBroadcaster";
                thread.Start();
            }
        }

        public static void Stop()
        {
            if (isRun)
            {
                isRun = false;
                thread.Join();
                thread = null;
            }
        }

        private static void run()
        {
            while (isRun)
            {
                try
                {
                    var start = DateTime.Now;
                    for (int i = 0; i < RoomManager.Count; i++)
                    {
                        RoomInstance room = RoomManager.Get(i);
                        if (room.bRranking)
                        {
                            var rankinglist = from w in room.score orderby w descending select w;
                            for (int j = 0; j < room.players.Count; ++j)
                            {
                                for(short num =0; num< rankinglist.Count(); ++num)
                                {
                                    if (rankinglist.ElementAt<int>(num) == room.players[j].score)
                                    {
                                        room.players[j].ranking = ++num;
                                        break;
                                    }
                                }
                            }
                            room.bRranking = false;
                        }
                        room.BroadcastRoomInfo();
                    }
                    Thread.Sleep(ServerSetting.TransformDataSendIntervalTime);
                    /*
                    if ((DateTime.Now - start).TotalMilliseconds < ServerSetting.TransformDataSendIntervalTime)
                    {
                        Thread.Sleep(DateTime.Now - start);
                    }
                    */
                }
                catch (Exception e)
                {
                    e.PrintStackTrace();
                }
            }
        }
    }
}