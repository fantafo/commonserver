﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using System;

namespace FTF.Server
{
    public class RoomManager
    {
        public static object LOCK = new object();
        static List<RoomInstance> rooms = new List<RoomInstance>();
        static Dictionary<long, RoomInstance> roomsDict = new Dictionary<long, RoomInstance>();
        public static event Action<RoomInstance> OnNewRoom;
        public static event Action<RoomInstance> OnRemoveRoom;

        static RoomManager()
        {
            TrackDataBroadcaster.Start();
        }

        public static void Add(RoomInstance room, bool isNewNumber = false)
        {
            lock (LOCK)
            {
                if(isNewNumber)
                {
                    room.Num = FindEmptyRoomNumber();
                }

                rooms.Add(room);
                roomsDict.Add(room.Num, room);
                rooms.Sort(roomCompare);
                room.OnCreated();

                TrackDataBroadcaster.Start();

                OnNewRoom?.Invoke(room);
            }
        }
        static System.Comparison<RoomInstance> roomCompare = new System.Comparison<RoomInstance>((a, b) => a.Num.CompareTo(b.Num));

        public static void Remove(RoomInstance room)
        {
            lock (LOCK)
            {
                rooms.Remove(room);
                roomsDict.Remove(room.Num);
                OnRemoveRoom?.Invoke(room);

                if (Count == 0)
                    TrackDataBroadcaster.Stop();
            }
        }
        public static int Count { get { return rooms.Count; } }
        public static RoomInstance Get(int index)
        {
            return rooms[index];
        }
        public static IEnumerator<RoomInstance> GetEnumerator()
        {
            lock (LOCK)
            {
                for (int i = 0; i < rooms.Count; i++)
                {
                    yield return rooms[i];
                }
            }
        }

        public static IEnumerable<RoomInstance> GetEnumerable()
        {
            return rooms;
        }


        public static RoomInstance GetNum(long roomNum)
        {
            RoomInstance room;
            if (roomsDict.TryGetValue(roomNum, out room))
            {
                return room;
            }
            return null;
        }
        public static bool Contains(long roomNum)
        {
            return roomsDict.ContainsKey(roomNum);
        }
        // 비어있는 방번호를 찾아온다.
        public static long FindEmptyRoomNumber(long expectNumber = 1)
        {
            if (expectNumber == 0)
            {
                expectNumber++;
            }
            for (int i = 0; i < rooms.Count; i++)
            {
                if (rooms[i].Num != expectNumber)
                {
                    break;
                }
                expectNumber++;
            }
            return expectNumber;
        }
    }
}