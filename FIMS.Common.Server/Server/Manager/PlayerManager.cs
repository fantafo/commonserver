﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTF.Server
{
    public static class PlayerManager
    {
        static object LOCK = new object();
        static List<PlayerInstance> players = new List<PlayerInstance>();

        public static void AddPlayer(PlayerInstance Player)
        {
            lock (LOCK)
            {
                players.Add(Player);
            }
        }

        public static void RemovePlayer(PlayerInstance Player)
        {
            lock (LOCK)
            {
                players.Remove(Player);
            }
        }

        public static IEnumerator<PlayerInstance> GetEnumerator()
        {
            lock (LOCK)
            {
                for (int i = 0; i < players.Count; i++)
                {
                    yield return players[i];
                }
            }
        }
        public static IEnumerable<PlayerInstance> GetEnumerable()
        {
            return players;
        }

        public static int Count { get { return players.Count; } }
        public static PlayerInstance Get(int index)
        {
            return players[index];
        }


        public static PlayerInstance GetId(int instanceID)
        {
            for (int i = 0; i < players.Count; i++)
            {
                if (players[i].instanceID == instanceID)
                {
                    return players[i];
                }
            }
            return null;
        }

    }
}
