﻿using UnityEngine;
using System.Collections.Generic;
using System.Text;
using FTF.Server.Packet;
using FTF;

namespace FTF.Server
{
    /// <summary>
    /// 계정을 관리하고 DB서버와 연계하여 계정정보와
    /// 캐릭터 정보 등을 설정한다.
    /// </summary>
    public class AccountManager
    {
        private static bool IsAutoLogin = true;
        private static Dictionary<long, Account> accounts = new Dictionary<long, Account>();
        private static Dictionary<string, Account> accountsID = new Dictionary<string, Account>();

        // SN만으로 계정 정보 생성.
        public static Account Create(Client client, long sn)
        {
            // 계정 인스턴스 생성
            Account acc;
            if (!accounts.TryGetValue(sn, out acc))
            {
                acc = new Account()
                {
                    SN = sn,
                };
                accounts.Add(sn, acc);
            }

            return AddAcc(client, acc);
        }


        // SN만으로 계정 정보를 가져온다.
        public static Account Login(Client client, long sn)
        {
            // 계정 인스턴스 생성
            Account acc;
            if (!accounts.TryGetValue(sn, out acc))
            {
                acc = new Account()
                {
                    SN = sn,
                };

                accounts.Add(sn, acc);
            }

            return LoginBackend(client, acc);
        }
        
        // SN과 이름만으로 계정을 대충 생성해낸다.
        public static Account Login(Client client, long sn, string name)
        {
            // 계정 인스턴스 생성
            Account acc;
            if (!accounts.TryGetValue(sn, out acc))
            {
                if (!accountsID.TryGetValue(name, out acc))
                {
                    acc = new Account()
                    {
                        SN = sn,
                        Name = name,
                        Password = name
                    };
                    accountsID.Add(name, acc);
                    accounts.Add(sn, acc);
                }
            }

            return LoginBackend(client, acc);
        }
        // ID, PW로 로그인을 시도한다.
        public static Account Login(Client client, string id, string pw)
        {
            // 계정 인스턴스 생성
            Account acc;
            if (!accountsID.TryGetValue(id, out acc))
            {
                acc = new Account()
                {
                    Id = id,
                    Name = id,
                    Password = pw
                };
                accountsID.Add(id, acc);
            }
            if (acc.Password == pw)
            {
                return LoginBackend(client, acc);
            }
            else
            {
                return null;
            }
        }
        // ID, PW로 로그인을 시도한다.
        public static Account LoginObserver(Client client)
        {
            // 계정 인스턴스 생성
            Account acc = new Account()
            {
                Id = "Observer",
                Name = "Observer",
                Password = "Observer"
            };
            return LoginBackend(client, acc);
        }
        private static Account LoginBackend(Client client, Account acc)
        {
            // 접속중인 유저가 있다면
            if (acc.client != null)
            {
                //TODO..
                //종료 후, accounts에서 정보를 제거 안하기 때문에 겹치는 문제.. 나중에 수정할것
                acc.client = null;
                //acc.client.Disconnect("같은 이름으로 접근한 유저가 있습니다.");
                //acc.client.Close();
            }

            // 플레이어 인스턴스 생성
            LoadPlayerInstance(acc);

            // 클라이언트에게 데이터 전송
            if (client != null)
            {
                acc.player.isLoaded = false;
                acc.player.IsReady = false;

                acc.client = client;
                client.account = acc;
                client.Send(S_Common.LoginResult(S_Common.LoginResultType.OK));
            }
            return acc;
        }

        //실제 로그인 과정 추가로 LoginBackend에서 분리함.. 
        //client에 acc 계정 설정
        private static Account AddAcc(Client client, Account acc)
        {
            // 접속중인 유저가 있다면
            if (acc.client != null)
            {
                //acc가 있다면 ID 입력이 틀려서 재로그인.
                return null;
                //acc.client.Disconnect("같은 이름으로 접근한 유저가 있습니다.");
                //acc.client.Close();
            }

            // 플레이어 인스턴스 생성
            LoadPlayerInstance(acc);

            // 클라이언트에게 데이터 전송
            if (client != null)
            {
                acc.player.isLoaded = false;
                acc.player.IsReady = false;

                acc.client = client;
                client.account = acc;
                //client.Send(S_Common.LoginResult(S_Common.LoginResultType.OK));
            }
            return acc;
        }

        public static void LoadPlayerInstance(Account acc)
        {
            if (acc.player == null)
            {
                acc.player = new PlayerInstance();
                acc.player.account = acc;
                acc.player.maxHP = 1;
                acc.player.currentHP = 1;

                PlayerManager.AddPlayer(acc.player);
            }
        }
    }
}