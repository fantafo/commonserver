﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FTF.Server
{
    public static class ClientManager
    {
        static object LOCK = new object();
        static List<Client> clients = new List<Client>();

        public static void AddClient(Client client)
        {
            lock (LOCK)
            {
                clients.Add(client);
            }
        }

        public static void RemoveClient(Client client)
        {
            lock (LOCK)
            {
                clients.Remove(client);
            }
        }

        public static IEnumerator<Client> GetEnumerator()
        {
            lock (LOCK)
            {
                for (int i = 0; i < clients.Count; i++)
                {
                    yield return clients[i];
                }
            }
        }

        public static Client Get(int index)
        {
            return clients[index];
        }

        public static int Count { get { return clients.Count; } }
    }
}
