﻿//#define SENDID

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using FTF.Server.Packet;
using FreeNet;

namespace FTF.Server
{
    /// <summary>
    /// 서버와 클라이언트가 하위레벨에서 소통하는 역할을 수행한다.
    /// </summary>
    public class Client : IPeer
    {

        public enum ClientType : byte
        {
            Client = 0,
            Editor = 1,
            Dummy = 2
        }
        public enum ClientState : byte
        {
            Disconnect,
            Login,
            Channel,
            Room,
            Loading,
            LoadComp,
            Started
        }


        /********************************************************************************/
        /*                                                                              */
        /*                               Member Variables                               */
        /*                                                                              */
        /********************************************************************************/

        CUserToken token;

        // default
        private static int clientIdFactory;
        public readonly int clientID = ++clientIdFactory;
        public SLog log = SLog._default;


        // Network
        private Socket socket;
        private NetworkStream stream;
        private BinaryReader reader;
        private BinaryWriter writer;
        public int pingCheckTime;
        public int pingCheckCount;
        public DateTime _pingTime;

        // Running 
        private bool IsRun = true;
        private Thread receiveThread;
        private Thread pingThread;

        // Handler
        public int version;
        public string identifier;
        private Cipher cipher;
        private PacketHandler packetHandler;

        // Account
        public Account account;
        public PlayerInstance player { get { return account != null ? account.player : null; } }

        public ClientState state = ClientState.Login;
        public ClientType _clientType;
        public bool IsEditor { get { return _clientType.HasFlag(ClientType.Editor); } }
        public bool IsDummy { get { return _clientType.HasFlag(ClientType.Dummy); } }

        /********************************************************************************/
        /*                                                                              */
        /*                               Constructor                                    */
        /*                                                                              */
        /********************************************************************************/

        public Client() { }
        //더이상 사용x
        public Client(Socket socket)
        {
            this.socket = socket;
            this.socket.LingerState = new LingerOption(true, 0);
            this.socket.ReceiveTimeout = ServerSetting.ClientReceiveTimeout;
            this.socket.SendTimeout = ServerSetting.ClientSendTimeout;
            this.stream = new NetworkStream(socket);
            this.reader = new BinaryReader(stream, Encoding.UTF8);
            this.writer = new BinaryWriter(stream, Encoding.UTF8);

            // 패킷핸들러 생성
            this.packetHandler = new PacketHandler(this);

            // 리딩 쓰레드를 생성한다.
            this.receiveThread = new Thread(_receiveRunnable);
            this.receiveThread.Name = string.Format("@Client-{0}-{1}", clientID, ((IPEndPoint)socket.RemoteEndPoint).Address);
            this.receiveThread.Start();

            // 핑을 보낼 쓰레드를 생성한다.
            this.pingThread = new Thread(_pingChecker);
            this.pingThread.Name = string.Format("@Ping-{0}-{1}", clientID, ((IPEndPoint)socket.RemoteEndPoint).Address);
            this.pingThread.Start();

            // 전용 로그를 생성한다.
            this.log = new SLog(ToString());

            // 매니저에 등록한다.
            ClientManager.AddClient(this);
        }

        public Client(CUserToken token)
        {
            // 패킷핸들러 생성
            this.packetHandler = new PacketHandler(this);
                        
            //FreeNet
            this.token = token;
            this.token.set_peer(this);

            // 전용 로그를 생성한다.
            //this.log = new SLog(ToString());

            // 매니저에 등록한다.
            ClientManager.AddClient(this);
            
        }

        void IPeer.on_removed()
        {
            //Console.WriteLine("The client disconnected.");
            Log("The client disconnected.");
            CommonServer.remove_user(this);
        }

        public void send(CPacket msg)
        {
            msg.record_size();
            this.token.send(new ArraySegment<byte>(msg.buffer, 0, msg.position));
        }

        public void send(ArraySegment<byte> data)
        {
            this.token.send(data);
        }

        public void send(byte[] data)
        {
            //Console.WriteLine("send data");
            this.token.send(new ArraySegment<byte>(data, 0, data.Length));
        }

        void IPeer.disconnect()
        {
            this.token.ban();
        }

        void IPeer.on_message(CPacket msg)
        {
            using (MemoryStream temp = new MemoryStream(msg.buffer, true))
            using (BinaryReader tReader = new BinaryReader(temp, Encoding.UTF8))
            {
                try
                {
                    //2바이트 읽고 버리기(size short형)
                    tReader.ReadH();
                    ClientOpcode protocol = (ClientOpcode)tReader.ReadClientCode();
                    //Console.WriteLine("protocol : " + protocol);

                    //if (protocol != ClientOpcode.C_TrackPosition && protocol != ClientOpcode.C_Command)
                    //Console.WriteLine($"===[Recv]=== {(protocol).ToString()}() : {msg.buffer.Length}byte\n{HexString.ToString(msg.buffer, msg.buffer.Length)}");

                    //if (protocol != ClientOpcode.C_TrackPosition)
                    //    Console.WriteLine($"===[Recv]=== {(protocol).ToString()}() : {msg.buffer.Length}byte\n{HexString.ToString(msg.buffer, msg.buffer.Length)}");

                    packetHandler.Handling(protocol, tReader);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }


        /********************************************************************************/
        /*                                                                              */
        /*                               Setting Functions                              */
        /*                                                                              */
        /********************************************************************************/

        //public virtual string IPAddress
        //{
        //    get
        //    {
        //        string ip = ((IPEndPoint)socket.RemoteEndPoint).Address.ToString();
        //        if (ip == "127.0.0.1" || ip == "0.0.0.0")
        //        {
        //            return ServerController.IPAddress;
        //        }
        //        else
        //        {
        //            return ip;
        //        }
        //    }
        //}

        public void SetSeed(byte[] seedPacket)
        {
            if (!IsRun)
                return;

            lock (this)
            {
                long seed = BitConverter.ToInt64(seedPacket, 14); // Use Start At 14 bytes.
                cipher = new Cipher(seed);
                writer.Write(seedPacket);

                LogFormat("Seed : {1:X}", socket.RemoteEndPoint, seed);
            }
        }


        /********************************************************************************/
        /*                                                                              */
        /*                               Network Functions                              */
        /*                                                                              */
        /********************************************************************************/
#if SENDID
        int sendID = 0;
#endif
        public virtual void Send(byte[] data, bool flush = true, bool throwing = false)
        {
            //더미 클라일 경우 socket이 없음
            if (!IsRun || socket == null)
                return;

#if SENDID
            Array.Resize(ref data, data.Length + 4);
            data[data.Length - 4] = (byte)(sendID & 0xFF);
            data[data.Length - 3] = (byte)((sendID>>0x08) & 0xFF);
            data[data.Length - 2] = (byte)((sendID>>0x10) & 0xFF);
            data[data.Length - 1] = (byte)((sendID>>0x18) & 0xFF);
#endif

            if (ServerSetting.ShowSendPacket)
            {
                switch ((ServerOpcode)data[0])
                {
                    case ServerOpcode.Room_Info:
                        break;
                    default:
                        LogFormat("===[Server]=== {0}({1:X2}) : {2}byte\n{3}", ((ServerOpcode)data[0]).ToString(), data[0], data.Length, HexString.ToString(data, data.Length));
                        break;
                }
            }
            lock (this)
            {
                try
                {
                    short packet_size = (short)(data.Length + sizeof(short));
                    byte[] packet = new byte[packet_size];
                    //패킷사이즈를 byte[]로 변환후 packet에 저장
                    BitConverter.GetBytes(packet_size).CopyTo(packet, 0);
                    data.CopyTo(packet, sizeof(short));
                    send(packet);
                    //Console.WriteLine(HexString.ToString(packet, packet.Length));
                    //if((ServerOpcode)packet[2] != ServerOpcode.Room_Info)
                       // Console.WriteLine($"===[Send]=== {((ServerOpcode)packet[2]).ToString()}() : {packet.Length}byte\n{HexString.ToString(packet, packet.Length)}");
                }
                catch (Exception e)
                {
                    if (IsRun)
                    {
                        LogException(e);
                        if (throwing)
                        {
                            throw e;
                        }
                    }
                }
            }
        }

        public void SendFlush()
        {
            if (!IsRun)
                return;

            lock (this)
            {
                writer.Flush();
            }
        }

        static byte[] pingBytes = S_Common.Ping();
        public virtual void SendPing()
        {
            if (!IsRun)
                return;

            lock (this)
            {
                writer.Flush();

                _pingTime = DateTime.Now;
                //Send(new byte[] { (byte)ServerOpcode.C_Ping });

                writer.WriteH(BasePacketExtension.OPCODE_SIZE);
                writer.WriteO(ServerOpcode.C_Ping);
                writer.Flush();
            }
        }

        public void Msg(string message)
        {
            LogWarning(message);
            Send(S_Common.Message(0, message));
        }

        public void Disconnect()
        {
            LogError("Request Disconnect ");
            try
            { Send(S_Common.Disconnect(S_Common.DisconnectType.Unkown)); }
            catch (Exception) { }
            try
            { socket.Dispose(); }
            catch (Exception e) { LogException(e); }
        }
        public void Disconnect(string message)
        {
            LogError("Request Disconnect " + message);
            try
            { Send(S_Common.Disconnect(message)); }
            catch (Exception) { }
            try
            { socket.Dispose(); }
            catch (Exception e) { LogException(e); }
        }
        public void Close()
        {
            if (IsRun)
            {
                IsRun = false;
                state = ClientState.Disconnect;

                if (account != null)
                    account.OnDisconnect();
                try
                { socket.Dispose(); }
                catch (Exception) { }
            }
            ClosePacketWriter();
        }


        /********************************************************************************/
        /*                                                                              */
        /*                               Runnable                                       */
        /*                                                                              */
        /********************************************************************************/

        internal byte[] buffer = new byte[ushort.MaxValue];
        internal int bufferSize = 0;
        void _receiveRunnable()
        {
            ClientOpcode code = ClientOpcode.None;
            int offset, size;

            using (MemoryStream temp = new MemoryStream(buffer, true))
            using (BinaryReader tReader = new BinaryReader(temp, Encoding.UTF8))
            {
                while (IsRun && ServerListener.IsRun)
                {
                    try
                    {
                        // 2bite의 사이즈를 읽어온다.
                        size = reader.ReadInt16();
                        offset = 0;

                        // 사이즈만큼의 데이터를 읽어온다.
                        while (size > 0)
                        {
                            int read = stream.Read(buffer, offset, Math.Min(size, 1024));
                            size -= read;
                            offset += read;
                        }
                        bufferSize = size = offset;
                        WritePacket($"Receive: {(ClientOpcode)buffer[0]}", buffer, 0, size, false);

                        try
                        {
                            temp.Position = 0;
                            code = tReader.ReadClientCode();

                            if (ServerSetting.ShowReceivePacket)
                            {
                                switch (code)
                                {
                                    case ClientOpcode.C_Ping:
                                    case ClientOpcode.C_TrackPosition:
                                    case ClientOpcode.C_VoIPSetup:
                                        break;
                                    default:
                                        LogFormat("===[Clinet]=== {0}({1:X2}) : {2}byte\n{3}", code.ToString(), (int)code, size, HexString.ToString(buffer, size));
                                        break;
                                }
                            }

                            size -= BasePacketExtension.OPCODE_SIZE;
                            packetHandler.Handling(code, tReader);
                        }
                        catch (Exception e)
                        {
                            LogErrorFormat("Exception Opcode {0}({1}) / Size: {2}byte\n{3}", code.ToString(), (short)code, size, HexString.ToString(buffer, size));
                            LogException(e);
                        }

                    }
                    catch (IOException e)
                    {
                        LogError("Socket Error " + e);
                        if (e.InnerException is SocketException)
                        {
                            LogError("Socket Error " + ((SocketException)e.InnerException).SocketErrorCode);
                        }
                        break;
                    }
                    catch (ObjectDisposedException e)
                    {
                        if (e.InnerException is SocketException)
                        {
                            LogError("Socket Error " + ((SocketException)e.InnerException).SocketErrorCode);
                        }
                        break;
                    }
                    catch (SocketException e)
                    {
                        LogError("Socket Error " + e.SocketErrorCode);
                        break;
                    }
                    catch (Exception e)
                    {
                        if (e.InnerException is SocketException)
                        {
                            LogError("Socket Error " + ((SocketException)e.InnerException).SocketErrorCode);
                            IsRun = false;
                            break;
                        }

                        if (IsRun && ServerListener.IsRun)
                        {
                            LogException(e);
                        }
                    }
                }
            }
            Log("Disconnect Complete");
            ClientManager.RemoveClient(this);
            Close();
        }

        void _pingChecker()
        {
            try
            {
                while (state < ClientState.Login)
                {
                    Thread.Sleep(1000);
                    if (IsRun && ServerListener.IsRun)
                    {
                        continue;
                    }
                    else
                    {
                        return;
                    }
                }

                int pingSkipCount = 0;
                int pingStartCount = -1;
                bool checkPingRespondingForExit = !_clientType.HasFlag(ClientType.Editor);
                while (IsRun && ServerListener.IsRun)
                {
                    Thread.Sleep(ServerSetting.ClientPingTiming);
                    if (checkPingRespondingForExit)
                    {
                        //클라가 핑을 보낼때 마다 pingCheckCount 하나씩 증가
                        if (pingStartCount == pingCheckCount)
                        {
                            if (++pingSkipCount == ServerSetting.ClientPingSkipError)
                            {
                                LogError("Ping Skip ERROR!!!!!!!!!!!!!!!");
                                Close();
                            }
                            if (pingSkipCount > 1)
                                SendPing();
                            continue;
                        }
                        else
                        {
                            pingSkipCount = 0;
                        }

                        pingStartCount = pingCheckCount;
                    }
                    SendPing();
                }
            }
            catch (Exception e)
            {
                if (IsRun && ServerListener.IsRun)
                {
                    LogException(e);
                }
            }
        }


        /********************************************************************************/
        /*                                                                              */
        /*                               ETC                                            */
        /*                                                                              */
        /********************************************************************************/
        //public override string ToString()
        //{
        //    string name = ((IPEndPoint)socket.RemoteEndPoint).Address.ToString();
        //    if (account != null)
        //        name += ":" + account.Name;
        //    name += ":" + clientID;
        //    return name;
        //}

        public override string ToString()
        {
            string name = ((IPEndPoint)this.token.socket.RemoteEndPoint).Address.ToString();
            if (account != null)
                name += ":" + account.Name;
            name += ":" + clientID;
            return name;
        }

        internal void RenameLog()
        {
            log.tag = ToString();
        }

        public void Log(string message)
        {
            log?.debug(message);
        }
        public void LogFormat(string message, params object[] objects)
        {
            Log(string.Format(message, objects));
        }
        public void LogError(string message)
        {
            log?.err(message);
        }
        public void LogErrorFormat(string message, params object[] objects)
        {
            LogError(string.Format(message, objects));
        }
        public void LogWarning(string message)
        {
            log?.warn(message);
        }
        public void LogWarningFormat(string message, params object[] objects)
        {
            LogWarning(string.Format(message, objects));
        }
        public void LogException(Exception e)
        {
            log?.ex(e);
        }

        /********************************************************************************/
        /*                                                                              */
        /*                               ETC                                            */
        /*                                                                              */
        /********************************************************************************/
        StreamWriter fsw;
        object writePacketLock = new object();
        void WritePacket(string msg, byte[] data, int index = 0, int size = -1, bool writeStack = true)
        {
            if (ServerSetting.SavePackets)
            {
                if (size == -1) size = data.Length;
                try
                {
                    lock (writePacketLock)
                    {
                        if (fsw == null)
                        {
                            var filename = $"packets/{((IPEndPoint)socket.RemoteEndPoint).Address.ToString().Replace(":", "")}/{DateTime.Now:yyyyMMdd_HHmmss}.txt";
                            var folder = Path.GetDirectoryName(filename);
                            if (!Directory.Exists(folder))
                                Directory.CreateDirectory(folder);

                            if (File.Exists(filename))
                                File.Delete(filename);

                            fsw = new StreamWriter(new FileStream(filename, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.ReadWrite), Encoding.UTF8);
                        }

                        fsw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        fsw.WriteLine($"[ {msg} ]  Size:{size}");
                        fsw.WriteLine(HexString.ToString(data, index, size));
                        if (writeStack && ServerSetting.SavePacketStack)
                        {
                            string[] stackTraces = Environment.StackTrace.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                            foreach (var stack in stackTraces)
                            {
                                if (stack.Contains(" System.Environment.")) ;
                                else
                                {
                                    fsw.WriteLine(stack);
                                }
                            }
                            fsw.WriteLine(" ");
                        }
                        fsw.Flush();
                    }
                }
                catch { }
            }
        }
        void ClosePacketWriter()
        {
            if (ServerSetting.SavePackets)
            {
                lock (writePacketLock)
                {
                    if (fsw != null)
                    {
                        try
                        {
                            fsw.Close();
                        }
                        catch { }
                    }
                }
            }
        }
    }
}
