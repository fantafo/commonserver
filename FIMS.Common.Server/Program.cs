﻿using FIMS.Common.Server;
using FTF.Server;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using FreeNet;

namespace FTF
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            //Common_server 단독 실행
            bool isNew = true;
            Mutex mutex = new Mutex(true, "common_server", out isNew);

            if (isNew == false)
            {
                return;
            }
            else
            {
                // 실행
                mutex.ReleaseMutex();
            }
            
            // WinForm용 코드
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            // 서버 설정 읽기
            ServerSetting.Load();

            // 서버 시작
            //_listening 스레드 생성, 접속 시 Client 생성

            //if (ServerListener.main.StartServer())
            {
                CommonServer.Start(ServerSetting.ServerPort);

                // Http 서버 및 커뮤니케이터 시작
                HttpListener.Start();
                HttpCommunicator.Start();

                // 콘솔 창에서 enter 키를 입력시 UI를 표시하기 위한실행 코드
                DateTime exitTime = new DateTime(0);
                while (true)
                {
                    Console.ReadLine();
                    if (exitTime < DateTime.Now && (ServerUI.main == null || !ServerUI.main.Visible))
                    {
                        Application.Run(new ServerUI());
                        exitTime = DateTime.Now.AddSeconds(1);
                    }
                }
            }

        }
    }
}