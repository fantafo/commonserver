﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

public class IpHelper
{
    static List<int> checkedAvailablePort = new List<int>();
    public static int AvailablePortRange(int portBegin, int portEnd)
    {
        // Evaluate current system tcp connections. This is the same information provided
        // by the netstat command line application, just in .Net strongly-typed object
        // form.  We will look through the list, and if our port we would like to use
        // in our TcpClient is occupied, we will set isAvailable to false.
        IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
        TcpConnectionInformation[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();

        for (int i = portBegin; i < portEnd; i++)
        {
            if (checkedAvailablePort.Contains(i))
                continue;

            foreach (var tcpi in tcpConnInfoArray)
            {
                if (tcpi.LocalEndPoint.Port == i)
                {
                    goto CONTINUE;
                }
            }
            checkedAvailablePort.Add(i);
            return i;

            CONTINUE:
            { }
        }
        return -1;
    }

    public static string InnerIP
    {
        get
        {
            string localIP = "Not available, please check your network seetings!";
            foreach (NetworkInterface ni in NetworkInterface.GetAllNetworkInterfaces())
            {
                var addr = ni.GetIPProperties().GatewayAddresses.FirstOrDefault();
                if (addr != null)
                {
                    if (ni.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || ni.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                    {
                        foreach (UnicastIPAddressInformation ip in ni.GetIPProperties().UnicastAddresses)
                        {
                            if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            {
                                localIP = ip.Address.ToString();
                                break;
                            }
                        }
                    }
                }
            }
            //가상화 사용시 ip주소가 다수 나옴. 
            /*
            IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                    break;
                }
            }
            */
            return localIP;
        }
    }

    public static string PublicIP
    {
        get
        {
            return new System.Net.WebClient().DownloadString("https://api.ipify.org");
        }
    }
}
