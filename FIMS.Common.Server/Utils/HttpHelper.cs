﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace FTF
{
    /// <summary>
    /// Http 프로토콜을 이용하여 데이터를 읽어올 수 있습니다.
    /// Json형태의 데이터만 읽어올 수 있으며
    /// 결과값은 dynamic으로 출력됩니다.
    /// 
    /// 대부분의 대상이 내부환경인 점을 감안하여 timeout은 500ms로 설정 돼 있습니다.
    /// 만약 대상이 외부환경일 경우에는 알맞은 timeout을 지정해야할 것입니다.
    /// </summary>
    public class HttpHelper
    {
        public static dynamic GetHTTP(string url, int timeout = -1)
        {
            return Process((client) => client.GetAsync($"http://{ServerSetting.FimsServerIP}:{ServerSetting.FimsServerPort}/{url}"), timeout);
        }
        public static dynamic Post(string url, string content, int timeout = -1)
        {
            return Process((client) =>
            {
                StringContent cnt = new StringContent(content, Encoding.UTF8);
                return client.PostAsync($"http://{ServerSetting.FimsServerIP}:{ServerSetting.FimsServerPort}/{url}", cnt);
            }, timeout);
        }

        public static dynamic Process(Func<HttpClient, Task<HttpResponseMessage>> GetMsgTask, int timeout)
        {
            if (timeout == -1)
                timeout = ServerSetting.HttpTimeout;

            if (timeout < int.MaxValue * 0.49f)
                timeout = (int)(timeout * ServerSetting.HttpTimeoutScale);
            else
                timeout = int.MaxValue;

            // connect game server
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    //서버에게 데이터를 JSON형식으로 전송할 것임을 알려줌
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    var msgTask = GetMsgTask(client);
                    msgTask.Wait(timeout);
                    if (msgTask.Status != System.Threading.Tasks.TaskStatus.RanToCompletion)
                        throw new HttpServerDontFoundException($"서버에서 장시간 응답하지 못했습니다. 관리자에게 문의해주세요.");

                    if (!msgTask.Result.IsSuccessStatusCode)
                        throw new HttpServerBadResponseException(Uri.UnescapeDataString(msgTask.Result.ReasonPhrase));

                    var task = msgTask.Result.Content.ReadAsStringAsync();
                    task.Wait(timeout);
                    if (task.Status != System.Threading.Tasks.TaskStatus.RanToCompletion)
                        throw new HttpServerDontFoundException($"서버에서 장시간 응답하지 못했습니다. 관리자에게 문의해주세요.");

                    dynamic result = new JavaScriptSerializer().Deserialize<dynamic>(task.Result);
                    //TODO: 추후 제거
                    //HACK: 이전버전의 호환성을 위해 남겨둔 코드
                    try
                    {
                        if (result != null && result["state"] != 0)
                            throw new HttpServerBadResponseException(result["msg"]);
                    }
                    catch (KeyNotFoundException e) { }
                    //HACK-END

                    return result;
                }
                catch (HttpServerBadResponseException e)
                {
                    throw e;
                }
                catch (HttpServerDontFoundException e)
                {
                    throw e;
                }
                catch
                {
                    throw new HttpServerDontFoundException("웹서버가 응답하지 않습니다. 관리자에게 문의해주세요");
                }
            }
        }
    }

    public class HttpServerException : Exception
    {
        public HttpServerException(string msg) : base(msg) { }
    }
    public class HttpServerDontFoundException : HttpServerException
    {
        public HttpServerDontFoundException(string msg) : base(msg) { }
    }
    public class HttpServerBadResponseException : HttpServerException
    {
        public HttpServerBadResponseException(string msg) : base(msg) { }
    }
}