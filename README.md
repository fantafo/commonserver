
이전개발자 Overview.txt
-개요
	Fims Common Server는 하나의 서버로 여러개의 다른 형태의 컨탠츠를 대응하기위함을 목적으로 제작됐습니다.
	그렇기에 특정 게임의 로직이 들어있거나 하진 않으며,
	FIMS 시스템의 방(세션) 구조를 구현할 수 있으며, 여러 유저들의 VR 트래킹을 주 목적으로 이루어져있습니다.
	게임별 특성에 맞는 패킷은 클라이언트에서 직접 제작하여 Command라는 형태로 전달하게되며,
	서버는 이것을 같은 방의 다른 유저에게 전달해주는 역할만을 수행합니다.

-패키지 구성
	Server				실제 서버 역할을 하는 패키지들이 포함돼있으며, Player, Room Instance와 게임구성용 매니저, 쓰레드, 패킷구성, 핸들링 들을 하는 요소들이 포함 돼 있다.
	Communicator		'FIMS Server'로부터 명령을 받아들이기 위한 Http Listener와, 서버의 상태를 보고하기위한 Http Communicator가 포함 돼 있다.
	Forms				서버를 UI로 컨트롤하기위한 WinForm
	
	SFramework			유니티의 SFramework의 일부를 차용해왔다. (편의성)
	Unity				유니티의 Vector3, Color등을 비슷하게 사용하기위해 임시로 만들었다. (편의성)
	Utils				메인에서 사용할 유틸용 클래스들이 몇개 포함 돼 있다.
	
	
옵저버 자동 실행.

설정방법.
Common 서버 실행파일(FIMS.Common.Server.exe )경로에 config\observerSetting.prop를 확인해 주세요.
해당 파일에서 옵저버 실행 경로를 설정합니다.
이름은 패키지명의 이름 부분을 입력해주세요.
반드시 패키지명은 com.fantafo.이름 형태로 되어야 합니다.

예시)
earth=F:\1.Build\earth\Observer\19_10_29\Earth.Observer.exe
cube=
uacon=F:\1.Build\urVR\Observer\19_10_29\uacon.Observer.exe
cnuh=
brain=
handleg=